/// @description
event_inherited();


// This should be overriden

logic = {

	name : "Indoor tree",
	description : "It's an indoor tree.",
	use_combine : false,
	// Custom attributes
	
	// Messages
	messages : [// TALK_TO,PUSH,PULL,USE,OPEN,CLOSE,PICK_UP,LOOK_AT,GIVE
		function() { return [""]; },
		function() { 		
			return ["It's too heavy."];
		},
		function() { return ["It's too heavy."]; },
		function() { return ["That doesn't make sense."]; },
		function() { return ["That doesn't make sense."]; },
		function() { return ["That doesn't make sense."]; },	
		function() { return ["I can't pick it up."]; },
		function() {
			var _array = [description];
			if (other.hidden_object) {
				array_push(_array, "Look, there's something inside!");	
				array_push(_array, "It's an old ID card, but the name has worn off.");	
			}
			return _array
		},
		function() { return [""]; }
	],
	// Allowed actions
	//					TALK_TO,PUSH,PULL,USE,OPEN,CLOSE,PICK_UP,LOOK_AT,GIVE
	allowed_actions : [true, true, true, true, true, true, false, true, true],
	
	actions : [// TALK_TO,PUSH,PULL,USE,OPEN,CLOSE,PICK_UP,LOOK_AT,GIVE
		function() {},
		function() {},
		function() {},
		function() {},
		function() {},
		function() {},
		function() {},
		function() {
			if (other.hidden_object) {
				var _id = instance_create_layer(-200, -200, "lyr_Instances", obj_IDCard);
				array_push(obj_Player.inventory, _id);
				other.hidden_object = false;
			}
		},
		function() {}
	]
}