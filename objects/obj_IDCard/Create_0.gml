/// @description
event_inherited();


// This should be overriden
image_index = 0;
logic = {

	name : "Blank ID card",
	description : "It's an ID card, but the name is not visible.",
	use_combine : true,
	// Custom attributes
	card_filled : false,
	
	// Messages
	messages : [// TALK_TO,PUSH,PULL,USE,OPEN,CLOSE,PICK_UP,LOOK_AT,GIVE
		function() { return [""]; },
		function() { 		
			return ["That doesn't make sense."];
		},
		function() { return ["That doesn't make sense."]; },
		function() { 
			if (Game.active_secondary_object.logic.name == "Marker") {
				return ["Ok, I've carefully written my name on it."];
			}
			else {
				return ["That doesn't make sense."];
			}
		},
		function() { return ["That doesn't make sense."]; },
		function() { return ["That doesn't make sense."]; },	
		function() { return [""]; },
		function() { return [description]; },
		function() { return ["Here's my ID card."]; }
	],
	// Allowed actions
	//					TALK_TO,PUSH,PULL,USE,OPEN,CLOSE,PICK_UP,LOOK_AT,GIVE
	allowed_actions : [true, true, true, true, true, true, false, true, true],
	
	actions : [// TALK_TO,PUSH,PULL,USE,OPEN,CLOSE,PICK_UP,LOOK_AT,GIVE
		function() {},
		function() {},
		function() {},
		function() {
			if (!card_filled) {
				if (Game.active_secondary_object.logic.name == "Marker") {
					image_index = 1;
					card_filled = true;
					name = "ID card";
					description = "It's an ID card and my name is written on it.";
					messages[COMMANDS.LOOK_AT] = function() { return ["It's an ID card and my name is written on it."]; };
				}
			}
		},
		function() {},
		function() {},
		function() {},
		function() {},
		function() {}
	]
}