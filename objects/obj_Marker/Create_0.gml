/// @description
event_inherited();


// This should be overriden
image_xscale = 0.5;
image_yscale = 0.5;
logic = {

	name : "Marker",
	description : "It's a black marker.",
	use_combine : true,
	// Custom attributes
	card_filled : false,
	
	// Messages
	messages : [// TALK_TO,PUSH,PULL,USE,OPEN,CLOSE,PICK_UP,LOOK_AT,GIVE
		function() { return [""]; },
		function() { 		
			return ["That doesn't make sense."];
		},
		function() { return ["That doesn't make sense."]; },
		function() { 
			if (Game.active_secondary_object.logic.name == "Blank ID card") {
				return ["Ok, I've carefully written my name on it."];
			}
			else {
				return ["That doesn't make sense."];
			}
		},
		function() { return ["That doesn't make sense."]; },
		function() { return ["That doesn't make sense."]; },	
		function() { return ["I'm sure they don't mind, they give away thousands of these."]; },
		function() { return [description]; },
		function() { return [""]; }
	],
	// Allowed actions
	//					TALK_TO,PUSH,PULL,USE,OPEN,CLOSE,PICK_UP,LOOK_AT,GIVE
	allowed_actions : [true, true, true, true, true, true, true, true, true],
	
	actions : [// TALK_TO,PUSH,PULL,USE,OPEN,CLOSE,PICK_UP,LOOK_AT,GIVE
		function() {},
		function() {},
		function() {},
		function() {
			if (!Game.active_secondary_object.logic.card_filled) {
				if (Game.active_secondary_object.logic.name == "Blank ID card") {
					Game.active_secondary_object.image_index = 1;
					Game.active_secondary_object.logic.name = "ID card";
					Game.active_secondary_object.card_filled = true;
					Game.active_secondary_object.description = "It's an ID card and my name is written on it.";
					Game.active_secondary_object.logic.messages[COMMANDS.LOOK_AT] = function() { return ["It's an ID card and my name is written on it."]; };
				}
			}
		},
		function() {},
		function() {},
		function() {},
		function() {},
		function() {}
	]
}