/// @description
event_inherited();


// This should be overriden

logic = {

	name : "Painting",
	description : "It's a painting from French artist Jacques Millard.",
	use_combine : false,
	// Custom attributes
	
	// Messages
		
	messages : [
		function() { return ["I won't talk to a painting!"]; },
		function() { 		
			return ["That doesn't make sense."];
		},
		function() { return ["That doesn't make sense."]; },
		function() { return ["Huh?"]; },
		function() { return ["I can't open that."]; },
		function() { return ["I can't close that."]; },	
		function() { return ["Ready."]; },
		function() { return [description]; },
		function() { return ["Huh?"]; }
	],
	// Allowed actions
	allowed_actions : [true, true, true, true, true, true, true, true, true],
	
	actions : [
		function() {},
		function() {},
		function() {},
		function() {},
		function() {},
		function() {},
		function() {},
		function() {},
		function() {}
	]
}