/// @description Global variables and data structures
#macro TYPE_FORMATTED_DEFAULT_ALPHA 1.0
#macro TYPE_FORMATTED_DEFAULT_COLOR c_black
#macro TYPE_FORMATTED_DEFAULT_HALIGN fa_left
#macro TYPE_FORMATTED_DEFAULT_VALIGN fa_top
#macro TYPE_FORMATTED_DEFAULT_XSCALE 1
#macro TYPE_FORMATTED_DEFAULT_YSCALE 1
#macro TYPE_FORMATTED_DEFAULT_FONT -1 // STRONGLY RECOMMENDED to substitute with your most used font. Use -1 for Arial 12, GMS2's default font, but beware this causes additional texture swaps
#macro TYPE_FORMATTED_ALPHA_CHANGE_TOLERANCE 0.05
#macro TYPE_FORMATTED_DEBUG_BBOX Game.debug
#macro TYPE_FORMATTED_DEBUG_NOTES false
#macro TYPE_FORMATTED_DEBUG_VERBOSE false
#macro TYPE_FORMATTED_AUTOFLUSH true
#macro TYPE_FORMATTED_CACHE_SIZE_FLUSH 150
#macro TYPE_FORMATTED_VERSION 0.5

enum TYPE_FORMATTED_TYPEWRITER {
	TYPEWRITER_DISABLED,
	TYPEWRITER_PAUSED,
	TYPEWRITER_RUNNING,
	TYPEWRITER_ENDED	
}

// Map
global.TypeFormatted_parsedElementMap = ds_map_create();

global.TypeFormatted_ParsedElement = function(_parsed_string_array, _sprite_array, _initial_subimage_array, _speed_array, _parameter_array, _bbox_width, _bbox_height, _halign, _valign, _initial_character, _typewriter_delay) constructor {
	// Keys
	parsed_string_array = _parsed_string_array;
	parameter_array = _parameter_array;
	
	sprite_array = _sprite_array;
	initial_subimage_array = _initial_subimage_array;
	speed_array = _speed_array;
	
	bbox_width = _bbox_width;
	bbox_height = _bbox_height;
	halign = _halign;
	valign = _valign;
	
	current_alarm = speed_array;
	current_subimg = _initial_subimage_array;
	
	current_character = _initial_character;
	typewriter_delay = _typewriter_delay;
	typewriter_alarm = _typewriter_delay;
	
	typewriter_paused = false;
	
	static setTypewriterDelay = function(_delay) {
		typewriter_delay = _delay;
		typewriter_alarm = _delay;
	}

	static restartTypewriter = function() {
		current_character = 0;
		typewriter_paused = false;
	}
	
	static endTypewriter = function() {
		current_character = totalCharacters();		
	}
	
	static pauseTypewriter = function() {
		typewriter_paused = true;
	}
	
	static resumeTypewriter = function() {
		typewriter_paused = false;
	}
	
	static getTypewriterStatus = function() {
		if (typewriter_delay == -1) {
			return TYPE_FORMATTED_TYPEWRITER.TYPEWRITER_DISABLED;
		}
		else if (typewriter_paused) {
			return TYPE_FORMATTED_TYPEWRITER.TYPEWRITER_PAUSED;
		}
		else if (current_character < totalCharacters()) {
			return TYPE_FORMATTED_TYPEWRITER.TYPEWRITER_ENDED;
		}		
		else {
			return TYPE_FORMATTED_TYPEWRITER.TYPEWRITER_RUNNING;
		}
			
	}
	
	static incrementFrame = function(_idx) {
		current_subimg[_idx] = (current_subimg[_idx]+1) % sprite_get_number(sprite_array[_idx]);
		current_alarm[_idx] = speed_array[_idx];
	}
	
	static incrementCharacter = function() {
		current_character = current_character+1;
		typewriter_alarm = typewriter_delay;
	}
	
	static totalCharacters = function() { // including "spaces" for sprites
		var _total = 0;
		var _n = array_length(parsed_string_array);		
		var _m = array_length(sprite_array);
		for (var _i=0; _i<_n; _i++) {
			if (string_copy(parsed_string_array[_i],1,1) != "@") {
				_total = _total + string_length(parsed_string_array[_i]);	
			}
		}		
		_total = _total + _m;
		return _total;
	}
	
	static getCurrentCharacter = function() {
		return current_character;
	}
	
	static debugString = function () {
		show_debug_message("Parsed string array: ");
		for (var _i=0; _i<array_length(parsed_string_array); _i++) {
			show_debug_message(parsed_string_array[_i]);
		}
		show_debug_message("Parsed parameter array: ");
		for (var _i=0; _i<array_length(parameter_array); _i++) {
			show_debug_message(parameter_array[_i]);
		}
	}
	
	
	bbox = function(_x, _y) {
		// Determine x1/x2 initial position for bbox and alignment
		if (halign == fa_left) {
			var _x1 = _x;
			var _x2 = _x + bbox_width;							
		}
		else if (halign == fa_right) {
			var _x1 = _x - bbox_width;	
			var _x2 = _x;
		}
		else {
			var _x1 = _x - bbox_width/2;
			var _x2 = _x + bbox_width/2;
		}
						
		// Determine bbox y1 and y2
		if (valign == fa_top) {
			var _y1 = _y;
			var _y2 = _y + bbox_height;
		}
		else if (valign == fa_bottom) {
			var _y1 = _y - bbox_height;
			var _y2 = _y;							
		}
		else {
			var _y1 = _y - bbox_height/2;
			var _y2 = _y + bbox_height/2;
		}
		
		return [_x1, _y1, _x2, _y2];
	}

}

// Color and alignment constants
event_perform(ev_other, ev_user0);

show_debug_message("TypeFormatted version "+string(TYPE_FORMATTED_VERSION)+" by biyectivo");