/// @description
event_inherited();


// This should be overriden

logic = {

	name : "Pot",
	description : "It's an empty flower pot.",
	use_combine : true,
	// Custom attributes

	
	// Messages
	messages : [// TALK_TO,PUSH,PULL,USE,OPEN,CLOSE,PICK_UP,LOOK_AT,GIVE
		function() { return ["I won't talk to an empty pot."]; },
		function() { 		
			return ["I don't want to push them."];
		},
		function() { return ["I'd rather not."]; },
		function() { 
			if (Game.active_secondary_object.logic.name == "Flower") {
				return ["Ok, I've planted the flower in the pot, looks great!"];
			}
			else {
				return ["That doesn't make sense"];	
			}
		},
		function() { return ["That doesn't make sense."]; },
		function() { return ["That doesn't make sense."]; },	
		function() { return ["I should return it later."]; },
		function() { return [description]; },
		function() { return [""]; }
	],
	// Allowed actions
	//					TALK_TO,PUSH,PULL,USE,OPEN,CLOSE,PICK_UP,LOOK_AT,GIVE
	allowed_actions : [true, true, true, true, true, true, true, true, true],
	
	actions : [// TALK_TO,PUSH,PULL,USE,OPEN,CLOSE,PICK_UP,LOOK_AT,GIVE
		function() {},
		function() {
		},
		function() {},
		function() {
			if (Game.active_secondary_object.logic.name == "Flower") {
				array_pop_resize(obj_Player.inventory, array_find(obj_Player.inventory, Game.active_secondary_object));
				array_pop_resize(obj_Player.inventory, array_find(obj_Player.inventory, Game.active_object));				
				var _id = instance_create_layer(-200, -200, "lyr_Instances", obj_WhiteFlowers);
				array_push(obj_Player.inventory, _id);				
			}
		},
		function() {},
		function() {},
		function() {},
		function() {},
		function() {}
	]
}