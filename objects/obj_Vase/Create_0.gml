/// @description
event_inherited();


// This should be overriden

logic = {

	name : "Vase",
	description : "It's a 19th century vase from the Ming dynasty. Must be worth millions!",
	use_combine : true,
	// Custom attributes
	broken : false,
	
	// Messages
	messages : [
		function() { return ["I won't talk to a vase!"]; },
		function() { 		
			return broken ? ["I'm not moving it more, I already broke it!"] : ["Oops! It broke."];
		},
		function() { return ["I'd rather not, it might break."]; },
		function() { return ["That doesn't make sense."]; },
		function() { return ["It's already open."]; },
		function() { return ["I can't, it has no lid."]; },	
		function() { return ["Ok."]; },
		function() { return [description]; },
		function() { return ["Huh?"]; }
	],
	// Allowed actions
	allowed_actions : [true, true, true, true, true, true, true, true, true],
	
	actions : [
		function() {},
		function() {
			broken = true;
			name = "Broken vase";
		
		},
		function() {},
		function() {},
		function() {},
		function() {},
		function() {},
		function() {},
		function() {}
	]
}