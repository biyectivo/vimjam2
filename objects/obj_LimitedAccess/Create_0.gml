/// @description
event_inherited();

sprite_index = asset_get_index("spr_LimitedAccess"+string(type));
// This should be overriden

logic = {

	name : "Rope Barrier",
	description : "It's a barrier restricting access to the elevator.",
	use_combine : false,
	// Custom attributes
	
	// Messages
	messages : [// TALK_TO,PUSH,PULL,USE,OPEN,CLOSE,PICK_UP,LOOK_AT,GIVE
		function() { return [""]; },
		function() { 		
			return ["I don't want to get into trouble."];
		},
		function() { return ["I'd rather not."]; },
		function() { return ["That doesn't make sense."]; },
		function() { return ["That doesn't make sense."]; },
		function() { return ["That doesn't make sense."]; },	
		function() { return ["I don't want to get into trouble."]; },
		function() { return [description]; },
		function() { return [""]; }
	],
	// Allowed actions
	//					TALK_TO,PUSH,PULL,USE,OPEN,CLOSE,PICK_UP,LOOK_AT,GIVE
	allowed_actions : [true, true, true, true, true, true, true, true, true],
	
	actions : [// TALK_TO,PUSH,PULL,USE,OPEN,CLOSE,PICK_UP,LOOK_AT,GIVE
		function() {},
		function() {},
		function() {},
		function() {},
		function() {},
		function() {},
		function() {},
		function() {},
		function() {}
	]
}