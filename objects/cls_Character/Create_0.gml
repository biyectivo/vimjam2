inventory = [];


talk_node = function(_id, _message, _response_option_ids) constructor {
	id = _id;
	dialog_text = _message;
	response_id = _response_option_ids;
	
	toString = function() {
		return string(id)+": "+dialog_text+" "+array_print(response_id);
	}
}


/// @description Create player
/*


#region Stats
	name = "Serena McGregor";
	max_hp = 1;
	hp = max_hp;
	
	player_speed = 4;	
#endregion



#region Other game-specific variables
	active_message = [];
	message_index = -1;
#endregion


// Technical stuff


#region FSM
	state = "Idle";
	substate = "Down";	
	last_substate = "";
	state_history = [];
	max_state_history = 16;
	
	last_state = function() {
		return (array_length(state_history) > 0) ? state_history[array_length(state_history)-1] : "";
	}
	
	is_ai = true;
	controllable = false;
	
	move_input = false;
	move_confirmed = false;
	move_angle = 0;
	path = noone;
	x_target = -1;
	y_target = -1;
	
	// State info
	state_speed = ds_map_create();
	ds_map_add(state_speed, "Idle", 10);
	ds_map_add(state_speed, "Move", 6);
	ds_map_add(state_speed, "PickUp", 10);
	
	
	// State functions
	state_machine = ds_map_create();
	
	ds_map_add(state_machine, "Idle", function() {
	
		// Get input
		fnc_GetInputOrSensors();	
		// Transition to other states
		fnc_Transition();		
	});
	
	ds_map_add(state_machine, "Move", function() {

		if (path_position == 0) {
			path_start(path, player_speed, path_action_stop, false);
		}
	
		if (path_position != 1) {
			move_angle = point_direction(x, y, x_target, y_target);
			substate = fnc_FSM_GetSubstateFromAngle(move_angle);
		}
	
		// Get input
		fnc_GetInputOrSensors();	
		// Transition to other states
		fnc_Transition();
	});

	fnc_GetInputOrSensors = function() {	
		
	}

	fnc_PerformMove = function(_x, _y) {
		x_target = _x;
		y_target = _y;
		var _id = instance_create_layer(_x-TILE_SIZE/2, _y-TILE_SIZE/2, "lyr_UI_In_Game_Below", obj_Ripple);
		if (path_exists(path)) {			
			path_delete(path);
		}
		path = path_add();			
		move_confirmed = mp_grid_path(Game.grid, path, x, y, _x, _y, true);
		if (move_confirmed) {
			path_position = 0;
			if (alarm[0] <= 0) {
				Game.active_command = noone;
			}
		}
		return move_confirmed;
	}


	fnc_Transition = function() {	
		// Transition matrix	
		if (state == "Idle") {		
			if (move_input && move_confirmed) state = "Move";	
		}
		else if (state == "Move") {
			if (path_position == 1) {
				state = "Idle";
				move_angle = 0;
				move_confirmed = false;
				x_target = -1;
				y_target = -1;
				path_position = 0;
				if (path_exists(path)) {			
					path_delete(path);
				}
			}
		}
		else { // Die
		
		}
	}



	// Helper functions
	fnc_FSM_GetSubstateFromAngle = function(_move_angle) {
		var _substate;
		if (_move_angle >= 0 && _move_angle <= 45 || _move_angle > 315 && _move_angle < 360) _substate = "Right";
		else if (_move_angle > 45 && _move_angle <= 135) _substate = "Up";
		else if (_move_angle > 135 && _move_angle <= 225) _substate = "Left";
		else _substate = "Down";
	
		return _substate;
	}

	fnc_FSM_IndexOfSubstate = function(_substate) {
		var _index;
		switch (_substate) {
			case "Right":	_index = 0; break;
			case "Up":		_index = 1; break;
			case "Left":	_index = 2; break;
			case "Down":	_index = 3; break;
			default:		_index = 0; break;
		}
		return _index;
	}

	fnc_FSM_ResetStateAnimation = function() {
		var _obj = string_copy(object_get_name(object_index), 5, string_length(object_get_name(object_index))-4);
		sprite_index = asset_get_index("spr_"+_obj+"_"+state);
		var _length = sprite_get_number(sprite_index)/4; // Refresh length with new sprite
		image_index = fnc_FSM_IndexOfSubstate(substate) * _length;
		Game.current_step = 0;
	}

	fnc_FSM_ResetSubstateAnimation = function() {
		var _length = sprite_get_number(sprite_index)/4; // Calculate existing
		image_index = fnc_FSM_IndexOfSubstate(substate) * _length;
		Game.current_step = 0;		
	}

	fnc_FSM_Animate = function() {
		var _length = sprite_get_number(sprite_index)/4; // Calculate existing
		var _speed = state_speed[? state];
		//show_debug_message("Else, step="+string(Game.current_step % _speed)+" spd="+string(_speed)+" length="+string(_length)+" image_index="+string(image_index)+" index="+string(fnc_obj_Player_FSM_IndexOfSubstate(substate)));
		if (Game.current_step % _speed == 0) {
			image_index = image_index + 1;
			var _index = fnc_FSM_IndexOfSubstate(substate);
			if (image_index == (_index+1)*_length) {
				//show_debug_message("resetting to "+string(_index*_length));
				image_index = _index * _length;
			}
		}
	}
	
	
	action_give = function() {
		if (Game.active_object.logic.name == "Vase") {
			array_print(inventory);
			array_pop_resize(obj_Player.inventory, array_find(obj_Player.inventory, Game.active_object));
			array_push(inventory, Game.active_object);			
			show_debug_message("Merci");
			array_print(inventory);
		}
		else {
			show_debug_message("No thanx");	
		}
		Game.active_object = noone;
		Game.active_command = noone;
	}
	
#endregion

*/