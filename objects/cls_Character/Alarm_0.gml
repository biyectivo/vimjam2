/// @description Active message
message_index++;
if (message_index < array_length(active_message)) {
	alarm[0] = Game.message_duration;
}
else {	
	message_index = -1;
	active_message = [];
	if (instance_exists(obj_TalkManager)) {
		obj_TalkManager.dialog_part_ended = true;
		//show_debug_message("ya acabé");
	}
}
