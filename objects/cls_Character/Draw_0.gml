/// @description Draw player
if (ENABLE_LIVE && live_call()) return live_result;
if (!Game.paused) {
	fnc_BackupDrawParams();
	
	// Shadow
	draw_set_alpha(0.3);
	draw_set_color(c_dkgray);
	draw_ellipse(x-sprite_width/2+5,y-10,x+sprite_width/2-5,y+10,false);
	draw_set_alpha(1);
	
	draw_self();
	
	// outfit
	var _obj = string_copy(object_get_name(object_index), 5, string_length(object_get_name(object_index))-4);
	draw_sprite_ext(asset_get_index("spr_"+_obj+"_Outfit_"+state), image_index, x, y, 1, 1, 0, c_white, 1);
	// hair
	draw_sprite_ext(asset_get_index("spr_"+_obj+"_Hairstyle_"+state), image_index, x, y, 1, 1, 0, c_white, 1);
	
	if (Game.debug) {
		draw_text(x, y-10, state+"/"+substate+"/"+string(image_index));
		draw_set_alpha(0.5);
		draw_rectangle_color(bbox_left, bbox_top, bbox_right, bbox_bottom, c_red, c_red, c_red, c_red, false);
		draw_set_alpha(1);		
	}
	
	
	// Messages
	if (array_length(active_message) != 0) {
		var _scale = 0.4;
		var _format = "[fnt_Commands][fa_bottom][fa_center][scale,"+string(_scale)+"]";
		var _color = dialog_color;
		var _color_shadow = "[c_black]";		
		type_formatted_shadow(x, y-sprite_height, _format+active_message[message_index], _color, _color_shadow);
	}
	else {
		// Mouseover text for NPC
		var _mousex = device_mouse_x(0);
		var _mousey = device_mouse_y(0);
		if (position_meeting(_mousex, _mousey, self) && is_ai) {
			var _scale = 0.4;
			var _y = y-TILE_SIZE*3/2-10;
			var _format = "[fnt_Commands][scale,"+string(_scale)+"][fa_center][fa_bottom]";
			type_formatted_shadow(x, _y, _format+string_copy(name, 1, string_pos(" ",name)-1), "[c_white]", "[c_black]");
		}
	}	
	
	fnc_RestoreDrawParams();
}