/// @description Execute FSM
if (!Game.paused) {
	if (ENABLE_LIVE && live_call()) return live_result;
	
	// Handle give, talk to
	
	var _mousex = device_mouse_x(0);
	var _mousey = device_mouse_y(0);
	if (is_ai && position_meeting(_mousex, _mousey, self) && device_mouse_check_button_pressed(0, mb_left)) {
		if (Game.active_object != noone && Game.active_command == COMMANDS.GIVE) {
			//show_debug_message("Player giving "+Game.active_object.logic.name+" to "+name);
			action_give();
		}
		else if (Game.active_object == noone && Game.active_command == COMMANDS.TALK_TO) {
						
			// Face each other
			var _angle = point_direction(x, y, obj_Player.x, obj_Player.y);
			substate = fnc_FSM_GetSubstateFromAngle(_angle);
			fnc_FSM_ResetStateAnimation();
			
			var _angle = point_direction(obj_Player.x, obj_Player.y, x, y);
			with (obj_Player) {
				substate = fnc_FSM_GetSubstateFromAngle(_angle);
				fnc_FSM_ResetStateAnimation();
			}
			
			// Initiate dialog
			var _id = instance_create_layer(-400, -400, "lyr_Instances", obj_TalkManager);
			with (obj_TalkManager) { // set up the controller with the corresponding dialog, actions and read marks
				dialog = other.dialog;
				dialog_read = other.dialog_read;
				dialog_actions = other.dialog_actions;
			}
		}
	}
	
	// Push previous state in queue and keep track of last substate
	if (last_state() != state) {
		array_push(state_history, state);			
		array_resize_from_end(state_history, max_state_history);		
	}
	last_substate = substate;	
	
	// Execute current FSM state
	/*var _state_function = asset_get_index("fnc_"+object_get_name(object_index)+"_FSM_"+state);
	
	if (script_exists(_state_function)) {
		_state_function();
	}*/
	state_machine[? state]();
	
	// Animate
	// At this point the state has transitioned.
	
	// Animation, Facing
	
	var _length = sprite_get_number(sprite_index)/4;
	
	if (last_state() != state) {
		//show_debug_message("Change of state");
		fnc_FSM_ResetStateAnimation();
	}
	else if (last_substate != substate) {
		//show_debug_message("Change of substate");
		fnc_FSM_ResetSubstateAnimation();
	}
	else {
		fnc_FSM_Animate();
	}
	
	
	
	// Active message
	
	if (array_length(active_message) != 0 && alarm[0] == -1) {		
		alarm[0] = Game.message_duration;
	}
	else if (array_length(active_message) != 0 && alarm[0] > 0 && keyboard_check_pressed(vk_space)) { // skip message
		alarm[0] = 1;
	}
	
	
	#region Lighting	
		/*
		occluder.x     = x;
		occluder.y     = y;
		occluder.angle = image_angle;


		with (flashlight) {
			x = other.x+10;
			y = other.y;
			angle = other.image_angle;	
		}
		if (keyboard_check_pressed(ord("F"))) {
			flashlight.visible = !flashlight.visible;
		}
		*/
	#endregion
	
	
}
