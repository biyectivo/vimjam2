/// @description	
if (!Game.paused) {
	if (ENABLE_LIVE && live_call()) return live_result;
	
	fnc_BackupDrawParams();
	draw_self();
	
	// Mouseover text
	var _mousex = device_mouse_x(0);
	var _mousey = device_mouse_y(0);
	if (position_meeting(_mousex, _mousey, self)) {
		var _scale = 0.4;
		var _y = y-TILE_SIZE*3/2-10;
		var _format = "[fnt_Commands][scale,"+string(_scale)+"][fa_center][fa_bottom]";
		type_formatted_shadow(x, _y, _format+logic.name, "[c_white]", "[c_black]");
	}
	
	fnc_RestoreDrawParams();
}