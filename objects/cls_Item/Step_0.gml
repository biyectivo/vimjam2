/// @description 
if (!Game.paused) {

	// Add to grid the first time it's created in the room
	if (!added_to_grid) {
		mp_grid_add_instances(Game.grid, self, false);
		added_to_grid = true;
	}

	// Process commands
	
	var _unreachable = choose("I can't reach it.", "It's too far away.", "It's out of reach.", "I'd need to get closer.");
	
	var _mousex = device_mouse_x(0);
	var _mousey = device_mouse_y(0);
	if ((position_meeting(_mousex, _mousey, self) && device_mouse_check_button_pressed(0, mb_left)) || simulate_mb_left) {		
		
		if (simulate_mb_left) simulate_mb_left = false;
		
		if (Game.active_command == COMMANDS.USE && Game.active_object != noone) { // Handle use combine
			Game.active_secondary_object = id;
			show_debug_message("Use "+Game.active_object.logic.name+" with "+Game.active_secondary_object.logic.name);
			
			// Trigger messages and actions of FIRST - calling - object (not current object)
			var _array = Game.active_object.logic.messages[COMMANDS.USE]();
			var _n = array_length(_array);
			for (var _i=0; _i<_n; _i++) array_push(obj_Player.active_message, _array[_i]);
			Game.active_object.logic.actions[COMMANDS.USE]();
			
			Game.active_object = noone;
			Game.active_secondary_object = noone;	
			Game.active_command = noone;
		}
		else {// Process normal switch statement if command different from use combine
			Game.active_object = id;
		
			// Reset current message
			if (Game.active_command != noone) {
				obj_Player.active_message = [];
				obj_Player.alarm[0] = -1;
			}
		
			switch (Game.active_command) {			
				case noone:
					break;
				case COMMANDS.TALK_TO:		
					var _array = logic.messages[COMMANDS.TALK_TO]();
					var _n = array_length(_array);
					for (var _i=0; _i<_n; _i++) array_push(obj_Player.active_message, _array[_i]);
					logic.actions[COMMANDS.TALK_TO]();
					break;
				case COMMANDS.PUSH:
					if (x<0 && y<0 || (distance_to_object(obj_Player) <= MIN_DISTANCE_TO_ACTION)) {
						var _array = logic.messages[COMMANDS.PUSH]();
						var _n = array_length(_array);
						for (var _i=0; _i<_n; _i++) array_push(obj_Player.active_message, _array[_i]);
						logic.actions[COMMANDS.PUSH]();
					}
					else {
						array_push(obj_Player.active_message, _unreachable);
					}
					break;
				case COMMANDS.PULL:
					if (x<0 && y<0 || (distance_to_object(obj_Player) <= MIN_DISTANCE_TO_ACTION)) {
						var _array = logic.messages[COMMANDS.PULL]();
						var _n = array_length(_array);
						for (var _i=0; _i<_n; _i++) array_push(obj_Player.active_message, _array[_i]);
						logic.actions[COMMANDS.PULL]();
					}
					else {
						array_push(obj_Player.active_message, _unreachable);
					}
					break;
				case COMMANDS.USE:
					if (x<0 && y<0 || (distance_to_object(obj_Player) <= MIN_DISTANCE_TO_ACTION)) {
						if (!logic.use_combine) { // Simple use
							var _array = logic.messages[COMMANDS.USE]();
							var _n = array_length(_array);
							for (var _i=0; _i<_n; _i++) array_push(obj_Player.active_message, _array[_i]);
							logic.actions[COMMANDS.USE]();
						}
						else {
							show_debug_message("Use "+Game.active_object.logic.name+" with...");
						}
					}
					else {
						array_push(obj_Player.active_message, _unreachable);
					}
					break;
				case COMMANDS.OPEN:
					if (x<0 && y<0 || (distance_to_object(obj_Player) <= MIN_DISTANCE_TO_ACTION)) {
						var _array = logic.messages[COMMANDS.OPEN]();
						var _n = array_length(_array);
						for (var _i=0; _i<_n; _i++) array_push(obj_Player.active_message, _array[_i]);
						logic.actions[COMMANDS.OPEN]();
					}
					else {
						array_push(obj_Player.active_message, _unreachable);
					}				
					break;
				case COMMANDS.CLOSE:
					if (x<0 && y<0 || (distance_to_object(obj_Player) <= MIN_DISTANCE_TO_ACTION)) {
						var _array = logic.messages[COMMANDS.CLOSE]();
						var _n = array_length(_array);
						for (var _i=0; _i<_n; _i++) array_push(obj_Player.active_message, _array[_i]);
						logic.actions[COMMANDS.CLOSE]();
					}
					else {
						array_push(obj_Player.active_message, _unreachable);
					}
					break;
				case COMMANDS.PICK_UP:
					if (logic.allowed_actions[COMMANDS.PICK_UP]) {
						if (x<0 && y<0 || (distance_to_object(obj_Player) <= MIN_DISTANCE_TO_ACTION)) {
							// Face object being picked up
							var _angle = point_direction(obj_Player.x, obj_Player.y, x, y);
							obj_Player.substate = obj_Player.fnc_FSM_GetSubstateFromAngle(_angle);
							obj_Player.fnc_FSM_ResetSubstateAnimation();
					
							// Trigger messages
							var _array = logic.messages[COMMANDS.PICK_UP]();
							var _n = array_length(_array);
							for (var _i=0; _i<_n; _i++) array_push(obj_Player.active_message, _array[_i]);
							logic.actions[COMMANDS.PICK_UP]();
					
							// Disable further pick up action
							logic.allowed_actions[COMMANDS.PICK_UP] = false;
							logic.messages[COMMANDS.PICK_UP] = function() {return [""];};
						
							// Add ID to inventory
							array_push(obj_Player.inventory, id);
						
							// Clear cell and move offscreen
							//mp_grid_clear_cell(Game.grid, floor(x/GRID_RESOLUTION), floor(y/GRID_RESOLUTION));
							mp_grid_clear_instance(Game.grid, id);
							Game.active_object = noone;
							x = -100;
							y = -100;
						}
						else {
							array_push(obj_Player.active_message, _unreachable);
						}
					}
					else {
						// Push messages anyway, use them to tell player they can't pick up this item
						var _array = logic.messages[COMMANDS.PICK_UP]();
						var _n = array_length(_array);
						for (var _i=0; _i<_n; _i++) array_push(obj_Player.active_message, _array[_i]);
						logic.messages[COMMANDS.PICK_UP]();
					}
					break;
				case COMMANDS.LOOK_AT:
					var _array = logic.messages[COMMANDS.LOOK_AT]();
					var _n = array_length(_array);
					for (var _i=0; _i<_n; _i++) array_push(obj_Player.active_message, _array[_i]);
					logic.actions[COMMANDS.LOOK_AT]();
					break;
				case COMMANDS.GIVE: // commenting this as give should not end in an object
				
					// Only allow giving things you are carrying
					if (!is_picked_up()) Game.active_object = noone;
					break;
			}
		
			if (Game.active_command != noone) {
				obj_Player.message_index = 0;
				if (obj_Player.alarm[0] != -1) {
					alarm[0] = -1;
				}
			
				if (Game.active_command != COMMANDS.GIVE && (Game.active_command != COMMANDS.USE || Game.active_command == COMMANDS.USE && !logic.use_combine)) {
					show_debug_message("Clear everything");
					Game.active_command = noone;
					Game.active_object = noone;
					Game.active_secondary_object = noone;
					Game.active_npc = noone;
				}
			}
		}
		
		
	}
	
}