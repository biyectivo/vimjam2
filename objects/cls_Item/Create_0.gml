/// @description 
added_to_grid = false;

simulate_mb_left = false;


is_picked_up = function() { return x < 0 && y < 0 };


// This should be overriden
/*

/// @description
event_inherited();


// This should be overriden
name = "Vase";
description = "It's a 19th century vase from the Ming dynasty. Must be worth millions!";

attributes = {
	broken : false
}

messages = {
	talkto :	function() { return ["I won't talk to a vase!"]; },
	push :		function() { 		
		return other.attributes.broken ? ["I'm not moving it more, I already broke it!"] : ["Oops! It broke."];
	},
	pull :		function() { return ["I'd rather not, it might break."]; },
	use :		function() { return ["That doesn't make sense."]; },
	open :		function() { return ["It's already open."]; },
	close :		function() { return ["I can't, it has no lid."]; },	
	pickup :	function() { return ["Ok"]; },
	lookat :	function() { return [other.description]; },
	give :		function() { return ["Huh?"]; }
}

allowed_actions = [];
allowed_actions[COMMANDS.TALK_TO] = true;
allowed_actions[COMMANDS.PUSH] = true;
allowed_actions[COMMANDS.PULL] = true;
allowed_actions[COMMANDS.USE] = true;
allowed_actions[COMMANDS.OPEN] = true;
allowed_actions[COMMANDS.CLOSE] = true;
allowed_actions[COMMANDS.PICK_UP] = true;
allowed_actions[COMMANDS.LOOK_AT] = true;
allowed_actions[COMMANDS.GIVE] = true;

actions = {
	talkto: function() {
		
	},
	push : function() {
		other.attributes.broken = true;
		other.name = "Broken vase";
		
	},
	pull : function() {
		
	},
	open : function() {
		
	},
	close : function() {
		
	},
	use : function() {
		
	},
	pickup : function() {
		
	},
	lookat : function() {
		
	},
	give : function() {
		
	}
}


*/