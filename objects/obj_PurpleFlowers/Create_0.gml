/// @description
event_inherited();


// This should be overriden

logic = {

	name : "Violets",
	description : "It's a pot with violets. Serena seems to love them.",
	use_combine : false,
	// Custom attributes
	
	// Messages
	messages : [// TALK_TO,PUSH,PULL,USE,OPEN,CLOSE,PICK_UP,LOOK_AT,GIVE
		function() { return ["Hi, pretty flowers!", "Not sure if they heard me."]; },
		function() { 		
			return ["I don't want to push them."];
		},
		function() { return ["I'd rather not."]; },
		function() { return ["That doesn't make sense."]; },
		function() { return ["That doesn't make sense."]; },
		function() { return ["That doesn't make sense."]; },	
		function() { return ["I don't want to get into trouble."]; },
		function() { return [description]; },
		function() { return [""]; }
	],
	// Allowed actions
	//					TALK_TO,PUSH,PULL,USE,OPEN,CLOSE,PICK_UP,LOOK_AT,GIVE
	allowed_actions : [true, true, true, true, true, true, false, true, true],
	
	actions : [// TALK_TO,PUSH,PULL,USE,OPEN,CLOSE,PICK_UP,LOOK_AT,GIVE
		function() {},
		function() {},
		function() {},
		function() {},
		function() {},
		function() {},
		function() {},
		function() {},
		function() {}
	]
}