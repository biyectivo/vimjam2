/// @description 
if (Game.debug) {
	fnc_BackupDrawParams();
	draw_set_alpha(0.3);
	mp_grid_draw(Game.grid);
	fnc_RestoreDrawParams();
}