//  Handle fullscreen change
if (keyboard_check(vk_lalt) && keyboard_check_pressed(vk_enter)) {
	fnc_SetGraphics();
}

// Fullscreen has changed (either via ALT+TAB or F10 on HTML5 or via menu)
if (fullscreen_change) {	
	window_set_fullscreen(option_value[? "Fullscreen"]);
	fullscreen_change = false;	
	// Update graphics
	fnc_SetGraphics();	
}

// Fullscreen toggle for HTML5
if (BROWSER && keyboard_check_pressed(ord("F"))) {
	//fullscreen_button = clickable_add(100, 100, sprite_get_tpe(spr_Button, 0), "gmcallback_fullscreen", "_self", "");
	/*var abc = ToggleFullScreen();
	window_set_fullscreen(abc == "true");
	if (abc == "true") {
		fnc_SetGraphics();
	}*/
	option_value[? "Fullscreen"] = !option_value[? "Fullscreen"];
	fnc_SetGraphics();
	
}


// Depth

var _inst_layer_depth = layer_get_depth(layer_get_id("lyr_Instances"));

with (all) {
	depth = (1-y/room_height) * _inst_layer_depth;
}

//show_debug_message(obj_Player.depth);

// Toggle GUI
if (keyboard_check_pressed(vk_tab)) {
	minimize_GUI = !minimize_GUI;	
}
