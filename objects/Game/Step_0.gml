if (ENABLE_LIVE && live_call()) return live_result;

if (!Game.lost && primary_gamepad != -1 && gamepad_button_check_pressed(primary_gamepad, gp_start)) {		
	event_perform(ev_keypress, vk_escape);
}

if (Game.lost && Game.paused && ((primary_gamepad != -1 && gamepad_button_check_pressed(primary_gamepad, gp_face1)) || (keyboard_check_pressed(vk_enter)))) {
	room_restart();
}

// Hide collision layer	
if (layer_exists(layer_get_id("lyr_Tile_Collision"))) {
	layer_set_visible(layer_get_id("lyr_Tile_Guides"), debug);
	layer_set_visible(layer_get_id("lyr_Tile_Collision"), debug);
}

// Room-specific code
if (!Game.paused) {
	if (room == room_Game_1) {
		
		// Register swipe/drag	
		device_mouse_dragswipe_register(0);
		
		// Global step
		if (instance_exists(obj_Player)) {
			current_step++;
		}
		
		var _name = active_command != noone ? command_names[active_command] : "";
		var _obj = active_object != noone ? active_object.logic.name : "";
		var _secondaryobj = active_secondary_object != noone ? active_secondary_object.logic.name : "";
		if (keyboard_check_pressed(vk_enter)) {
			show_debug_message("Active command: "+_name+" Active object: "+_obj+" Active secondary object: "+_secondaryobj);
		}
	}	
}


