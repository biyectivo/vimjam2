/// @description
event_inherited();


// This should be overriden

logic = {

	name : "Statue of CEO",
	description : "Mr. Robert Craig, CEO, The Edge Corporation.",
	use_combine : false,
	// Custom attributes
	
	// Messages
	messages : [// TALK_TO,PUSH,PULL,USE,OPEN,CLOSE,PICK_UP,LOOK_AT,GIVE
		function() { return [""]; },
		function() { 		
			return ["It's extremely heavy."];
		},
		function() { return ["It's extremely heavy."]; },
		function() { return ["That doesn't make sense."]; },
		function() { return ["That doesn't make sense."]; },
		function() { return ["That doesn't make sense."]; },	
		function() { return ["It's too heavy."]; },
		function() { return [description, "Wow, what a self-centered guy!", "If I ever become CEO, I will not do this."]; },
		function() { return [""]; }
	],
	// Allowed actions
	//					TALK_TO,PUSH,PULL,USE,OPEN,CLOSE,PICK_UP,LOOK_AT,GIVE
	allowed_actions : [true, true, true, true, true, true, false, true, true],
	
	actions : [// TALK_TO,PUSH,PULL,USE,OPEN,CLOSE,PICK_UP,LOOK_AT,GIVE
		function() {},
		function() {},
		function() {},
		function() {},
		function() {},
		function() {},
		function() {},
		function() {},
		function() {}
	]
}