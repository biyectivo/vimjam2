/// @description 
if (!Game.paused) {
	if (distance_to_object(obj_Player) < 2*TILE_SIZE) {
		if (closed) {
			if (alarm[0] == -1) {
				alarm[0] = door_speed;
			}
		}
	}
	else {
		if (!closed) {
			if (alarm[1] == -1) {
				alarm[1] = door_speed;
			}
		}
	}
}