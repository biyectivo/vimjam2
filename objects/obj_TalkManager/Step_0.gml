/// @description 
if (ENABLE_LIVE && live_call()) return live_result;
if (!Game.paused) {
	
	if (dialog_part_ended) {
		
		if (current_dialog_step == -1) { // End of dialog object
			//show_debug_message("End of dialog");
			obj_Player.controllable = true;	
			instance_destroy();
		}
		else if (array_length(dialog[current_dialog_step].response_id()) == 1) { // There's a single reply, go ahead and push it, then move to the next step						
			var _array = dialog[current_dialog_step].response_id();
			current_dialog_step = _array[0];			
			//show_debug_message("Single reply, "+string(_array[0]));
			if (_array[0] != -1) {
				fnc_PushMessage(current_dialog_step);
			
				// Execute actions
				dialog_actions[current_dialog_step]();
			}
		}
		else if (reply_from_player != noone) { // Player has chosen
			//show_debug_message("Player has chosen "+string(reply_from_player) );	
			var _array = dialog[current_dialog_step].response_id();
			current_dialog_step = reply_from_player;
			
			// Mark as read
			var _id = dialog[current_dialog_step].id;	
			_id.dialog_read[reply_from_player] = true;
			dialog_read[reply_from_player] = true;
			
			show_dialog_gui = false;
			fnc_PushMessage(current_dialog_step);
			
			// Execute actions
			dialog_actions[current_dialog_step]();
			
			reply_from_player = noone;
		}
		else { // Show GUI
			show_dialog_gui = true;
		}
		
	}
		
}