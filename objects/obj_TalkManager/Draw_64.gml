/// @description 
if (ENABLE_LIVE && live_call()) return live_result;
if (!Game.paused) {	
	// Draw GUI Dialog options
	if (array_length(dialog)>0 && show_dialog_gui && reply_from_player == noone) {
		
		var _num_items = array_length(dialog[current_dialog_step].response_id())
		
		var _panel_height = max(150, _num_items * 50);
		
		var _w = GUI_WIDTH;
		var _h = GUI_HEIGHT;
		var _padding = 10;
		var _bgcolor = $222222;
		var _mgcolor = $111111;
		
		draw_set_alpha(0.85);
		draw_roundrect_color(_padding, _h-_panel_height-_padding, _w-_padding, _h-_padding, _bgcolor, _bgcolor, false);
		draw_set_alpha(1);
		draw_roundrect_color(_padding, _h-_panel_height-_padding, _w-_padding, _h-_padding, _mgcolor, _mgcolor, true);
	
		var _x1 = 6*_padding;
		var _y1 = _h-_panel_height+_padding;
		var _scale = 0.3;
	
		var _color_read = "[c_dkgray2]";
		var _color_mouseover = "[c_white]";
		var _color_normal = "[c_silver]";
		
		var _format_read = "[fnt_Commands][fa_top][fa_left][scale,"+string(_scale)+"]";
		var _format_mouseover = "[fnt_Commands][fa_top][fa_left][scale,"+string(_scale)+"]";		
		var _format_normal = "[fnt_Commands][fa_top][fa_left][scale,"+string(_scale)+"]";
		
		var _array = dialog[current_dialog_step].response_id();
		var _n = array_length(_array);
		for (var _i=0; _i<_n; _i++) {
			var _array_messages = dialog[_array[_i]].dialog_text();
			var _already_selected = dialog_read[_array[_i]];
			fnc_Link(_x1, _y1+40*_i, (_already_selected ? _color_read : _color_normal)+_format_read+_array_messages[0],	_color_mouseover+_format_mouseover+_array_messages[0], fnc_ProcessDialog, _array[_i]);			
		}
		
	}
}