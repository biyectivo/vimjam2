/// @description
event_inherited();


// This should be overriden

logic = {

	name : "Flower",
	description : "It's a beautiful white flower.",
	use_combine : true,
	// Custom attributes
	
	// Messages
	messages : [// TALK_TO,PUSH,PULL,USE,OPEN,CLOSE,PICK_UP,LOOK_AT,GIVE
		function() { return [""]; },
		function() { 		
			return ["I don't think so."];
		},
		function() { return ["I'd rather not."]; },
		function() { 
			if (Game.active_secondary_object.logic.name == "Pot") {
				return ["Ok, I've planted the flower in the pot, looks great!"];
			}
			else {
				return ["That doesn't make sense"];	
			}
		},
		function() { return ["That doesn't make sense."]; },
		function() { return ["That doesn't make sense."]; },	
		function() { return ["This might look good on exhibition."]; },
		function() { return [description]; },
		function() { return [""]; }
	],
	// Allowed actions
	//					TALK_TO,PUSH,PULL,USE,OPEN,CLOSE,PICK_UP,LOOK_AT,GIVE
	allowed_actions : [true, true, true, true, true, true, true, true, true],
	
	actions : [// TALK_TO,PUSH,PULL,USE,OPEN,CLOSE,PICK_UP,LOOK_AT,GIVE
		function() {},
		function() {},
		function() {},
		function() {
			if (Game.active_secondary_object.logic.name == "Pot") {
				array_pop_resize(obj_Player.inventory, array_find(obj_Player.inventory, Game.active_secondary_object));
				array_pop_resize(obj_Player.inventory, array_find(obj_Player.inventory, Game.active_object));				
				var _id = instance_create_layer(-200, -200, "lyr_Instances", obj_WhiteFlowers);
				array_push(obj_Player.inventory, _id);
			}
		},
		function() {},
		function() {},
		function() {},
		function() {},
		function() {}
	]
}