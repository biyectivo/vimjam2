/// @description
event_inherited();


// This should be overriden

logic = {

	name : "Cheap Vase",
	description : "It's just a vase someone surely ordered from Amazon.",
	use_combine : true,
	// Custom attributes
	broken : false,
	
	// Messages
	messages : [ // TALK_TO,PUSH,PULL,USE,OPEN,CLOSE,PICK_UP,LOOK_AT,GIVE
		function() { return ["I won't talk to a cheap vase!"]; },
		function() { 		
			return broken ? ["I'm not moving it more, I already broke it!"] : ["Oops! It broke."];
		},
		function() { return ["I'd rather not, it might break."]; },
		function() { return ["That doesn't make sense."]; },
		function() { return ["It's already open."]; },
		function() { return ["I can't, it has no lid."]; },	
		function() { return ["Ok."]; },
		function() { return [description]; },
		function() { return ["Huh?"]; }
	],
	// Allowed actions
	//				TALK_TO,PUSH,PULL,USE,OPEN,CLOSE,PICK_UP,LOOK_AT,GIVE
	allowed_actions : [true, true, true, true, true, true, true, true, true],
	
	actions : [// TALK_TO,PUSH,PULL,USE,OPEN,CLOSE,PICK_UP,LOOK_AT,GIVE
		function() {},
		function() {
			broken = true;
			name = "Broken, cheap vase";
		
		},
		function() {},
		function() {
			if (Game.active_secondary_object.logic.name == "Painting") {
				if (Game.active_object.is_picked_up() && Game.active_secondary_object.is_picked_up()) {
					show_debug_message("Both "+Game.active_object.logic.name+" and "+Game.active_secondary_object.logic.name+" have been picked up");
					var _id = instance_create_layer(-300, -300, "lyr_Instances", obj_Vase);					
					
					// Remove other objects from inventory
					array_pop_resize(obj_Player.inventory, array_find(obj_Player.inventory, Game.active_object));
					array_pop_resize(obj_Player.inventory, array_find(obj_Player.inventory, Game.active_secondary_object));
					
					Game.active_secondary_object = noone;
					Game.active_object = noone;
					Game.active_command = noone;
					
					// Add to inventory
					array_push(obj_Player.inventory, _id);
				}
				else if (Game.active_object.is_picked_up() && !Game.active_secondary_object.is_picked_up()) {
					show_debug_message(Game.active_object.logic.name+" has been picked up and "+Game.active_secondary_object.logic.name+" exists");
				}
				else if (!Game.active_object.is_picked_up() && Game.active_secondary_object.is_picked_up()) {
					show_debug_message(Game.active_object.logic.name+" exists and "+Game.active_secondary_object.logic.name+" has been picked up");
				}			
				else {
					show_debug_message("Both "+Game.active_object.logic.name+" and "+Game.active_secondary_object.logic.name+" haven't been picked up");
					var _id = instance_create_layer(-300, -300, "lyr_Instances", obj_Vase);
					
					mp_grid_clear_instance(Game.grid, Game.active_secondary_object);
					mp_grid_clear_instance(Game.grid, Game.active_object);
					var _x = Game.active_secondary_object.x;
					var _y = Game.active_secondary_object.y;
					Game.active_secondary_object.x = -200;
					Game.active_secondary_object.y = -200;
					Game.active_object.x = -200;
					Game.active_object.y = -200;
					
					Game.active_secondary_object = noone;
					Game.active_object = noone;
					Game.active_command = noone;
					
					// Move object to correct position and add to grid
					_id.x = _x;
					_id.y = _y;
					mp_grid_add_instances(Game.grid, _id, false);
				}
			}
		},
		function() {},
		function() {},
		function() {},
		function() {},
		function() {}
	]
}