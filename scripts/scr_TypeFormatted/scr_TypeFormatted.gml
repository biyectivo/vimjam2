#region Documentation
/***********************************************************************************************************************
	
											___        _                         
											 |  ._  _ |__ ._._ _  _._|__|_ _  _| 
											 |\/|_)(/_|(_)| | | |(_| |_ |_(/_(_| 
											  / |                                
	
											by José Alberto Bonilla Vera (biyectivo)
										biyectivo@gmail.com / Discord tag biyectivo#2771
	
							The meh developer's version of JujuAdams's fabulous Scribble asset.
									"Terrible performance, but it doesn't crash in HTML5!"

	~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	
	-------
	 INTRO
	-------
	
	TypeFormatted is a simple asset that allows you to draw formatted text and inline sprites in a single call. It's
	heavily inspired by the marvelous JujuAdams's Scribble asset (https://github.com/JujuAdams/Scribble). It performs
	WAY slower than Scribble, but the main reason I developed this is because Scribble does not work on HTML5 exports
	due to a bug in Gamemaker's Javascript runner.
	
	While Scribble does vertex buffer magic under the hood, TypeFormatted simply uses Gamemaker's built-in functions to
	draw stuff for you.
	
	TypeFormatted supports different formatting tags for changing color, alignment, alpha, blend, font, inline sprites
	and a basic typewriter animation. It also barely supports rotated drawing (with some bugs).
	
	TypeFormatted does NOT support multiline text, sounds and other effects.
	
	
	---------
	 CAVEATS
	---------
	
	TypeFormatted WILL drop your framerate significantly, because it uses built-in functions to draw and data structures
	to parse. Also, it is currently not very optimized.
	
	TypeFormatted requires Gamemaker Studio 2.3 or greater.
	
	Also check the Known Bugs section before using this.
	
	Use this asset at your own risk (and performance drop). If you don't need an HTML5 export, I highly recommend
	using JujuAdams's Scribble instead.
	
	
	-------
	 USAGE
	-------
	
	1) Add obj_TypeFormatted to your init room.
	2) Use the following function:
	
	
							struct = type_formatted(x, y, string <,draw, start_character, speed>);
	
	------------
	 PARAMETERS
	------------
	
	x - x position to draw
	y - y position to draw
	string - text string with different format tags, as such:
		
		\\							Escape character. The following character will be interpreted verbatim.
		
									Example:
									\\[					(Will print an open bracket)
									
									Note, you cannot escape a backward slash at the moment - use [chr,92] instead.
		
		[]							Reset all formatting to default. You can set the default formatting options
									by modifying the macros inside obj_TypeFormatted's Create event.
									
		[c_xxxxxxxx]				Change color of the following characters or sprite's blend color, by using 
									etiher a Gamemaker color constant (c_...) or the additional color constants
									included in this asset.
									Check out the list of additional colors available in obj_TypeFormatted's 
									User Event 0.
							
									Examples:
									[c_red]				(Gamemaker's built-in red constant)
									[c_dksalmon]		(TypeFormatted's dark salmon constant)
									
		[c_random]					Choose a random color (every frame!) to draw the following character.
		
		[$xxxxxx]					Change color of the following characters using a Gamemaker's custom color (BGR).
									
									Examples:
									[$00ccff]			(Gamemaker's gold color)
									
		[angle, x]					Change the angle at which following text or sprites are drawn, x between
									0 and 360.
							
									Examples:
									[angle, 45]			(Draw at a 45° angle)
									
		[blend, xxxxxx]				Change blend color of the following sprites using either a Gamemaker's built-in 
									color constant, a Gamemaker's custom BGR color or a hex color.
									
									Examples:
									[blend, #999999]	(Draw sprites using the hex gray color)
		
		[alpha, x]					Change alpha of the following text or sprites; x must be between 0 and 1.
							
									Examples:
									[alpha, 0.8]		(Draw at 80% opacity)
									
		[scale, x <,y>]				Change scale of the following text or sprites; specify x only to scale 
									proportionaly both width and height; specify x and y to scale at different
									proportions.
							
									Examples:
									[scale, 2]			(Draw at 2x size proportionally)
									[scale, 1.5, 3]		(Draw width scaled at 1.5x and height scaled at 3x)
		
		[chr, x]					Insert a character based on its ASCII code x, where x must be a valid ASCII
									code (32-126).
									
									Example:
									[chr,92]			(Draw a single backslash character)
		
		[fa_xxxxxx]					Change horizontal or vertical alignment for the entire string, using the 
									Gamemaker's built-in alignment constants. If multiple horizontal or vertical
									tags are present in the string, only the last horizontal alignment and vertical
									alignment tags are taken into account.
									
									Examples:
									[fa_bottom]			(Draw everything vertically aligned to the bottom baseline)
									[fa_right]			(draw everything horizontally aligned to the right)
									
		[sprite <,idx, speed>]		Draw the requested sprite inline with the rest of the text. If only the sprite's
									name is used, it will draw frame 0 of the sprite, without animation; if a starting
									frame is specified, it will draw that frame instead and animate using the sprite
									speed defined in the sprite properties dialog; finally, if both a starting frame 
									and speed are specified, it will animate the sprite, starting at the specified 
									frame and animating every <speed> game steps.
									
									Examples:
									[spr_Player]		(Draw the first frame of the spr_Player sprite, statically)
									[spr_Player,2]		(Draw the third frame of the spr_Player sprite, animating
														at the speed set in the sprite properties)
									[spr_Player,2,8]	(Draw the spr_Player sprite and animate it, starting with
														frame 2 and animating it every 8 game steps, i.e., at
														8/60 frames per second for a game with Game Speed set at 60)
		

	<draw> -	optional boolean value that sets whether to actually draw the string or just add it to the cache (and
				return the struct).
	<start_character> - optional argument for typewriter mode; sets the index of the character to start drawing at.
	<speed> - optional argument for typewriter mode; sets the speed in game steps to draw text at.
	
	
	--------
	 OUTPUT
	--------
	
	A struct (of type global.TypeFormatted_ParsedElement) containing a lot of info.
	What is usable about the output struct is the following:
	
	"Public" Attributes:
	
	bbox_width - The total width of the bounding box of the parsed string.
	bbox_height - The total height of the bounding box of the parsed string.
	halign - The horizontal alignment considered for the parsed string.
	valign - The vertical alignment considered for the parsed string.
	
	"Public" Methods:
	
	setTypewriterDelay(x)	Sets the typewriter effect's delay between each character/sprite, x being game steps.
	restartTypewriter()		Restarts the typewriter at the first character/sprite.
	endTypewriter()			Ends the typewriter effect and renders the complete string.		
	totalCharacters()		counts the number of characters in a parsed string, adding 1 for each sprite.
	debugString()			Prints the parsed string in Gamemaker's log
	bbox(x, y)				Return an array with the x1, y1, x2 and y2 coordinates of the bounding box, given
							the x and y positions where the string was/is going to be drawn.
	
	
	----------
	 EXAMPLES
	----------
	
	
	
	-------
	 CACHE
	-------
	
	TypeFormatted uses a ds_map to cache parsed strings in order to attempt to speed up the drawing each frame.
	However, if the cache gets too big, the balance between the performance gain to parse the string and
	the performance loss given by the size of the map will be broken. You can set the maximum size of the 
	cache and let TypeFormatted flush it automatically at that size, or you can also flush the cache manually.
	
	To set the maximum size of the cache and let TypeFormatted flush the cache automatically:
	
	1) Set the macro TYPE_FORMATTED_AUTOFLUSH to true, within obj_TypeFormatted.
	2) Set the macro TYPE_FORMATTED_CACHE_SIZE_FLUSH to the desired limit, within obj_TypeFormatted.
	
	To manually flush the cache:
	
	1) Call fnc_TypeFormatted_Flush() whenever needed.
	
	Important:
	
	If you are drawing a lot of dynamically built strings, you'll want to flush the cache (either automatically or
	manually), since every string will be different and thus will be parsed. For example, if you are trying to
	draw the game score, every time the player does something to increase or decrease score the string will change,
	thus TypeFormatted will have to parse it again, and will store all versions of the string in the cache.
	
	You can also get the size of the cache by calling fnc_TypeFormatted_CacheSize();
	
	------------------------------
	 OTHER USEFUL MACRO VARIABLES
	------------------------------
	
	Default draw settings:
	
	TYPE_FORMATTED_DEFAULT_ALPHA 				Default value for alpha
	TYPE_FORMATTED_DEFAULT_COLOR 				Default value for color
	TYPE_FORMATTED_DEFAULT_HALIGN				Default value for horizontal alignment
	TYPE_FORMATTED_DEFAULT_VALIGN				Default value for vertical alignment
	TYPE_FORMATTED_DEFAULT_XSCALE				Default value for x scaling
	TYPE_FORMATTED_DEFAULT_YSCALE				Default value for y scaling
	TYPE_FORMATTED_DEFAULT_FONT					Default font. If you are using a base font for you project, it's 
												strongly recommended to set the default font to your font, to avoid
												unnecesary swaps.
	
	Other settings:
	
	TYPE_FORMATTED_ALPHA_CHANGE_TOLERANCE		Threshold under which an alpha change will not be processed.
												For example, if this value is 0.05 and you change alpha from 1.0 to
												0.96, since the difference is less than 0.05 it will not be changed, to
												avoid additional swaps.
	TYPE_FORMATTED_DEBUG_BBOX					Set to true to visually see the bounding box overlaid in top of your
												drawn string.
	TYPE_FORMATTED_DEBUG_NOTES					Set to true to display notes in Gamemaker's log.
	TYPE_FORMATTED_DEBUG_VERBOSE				Set to ture to display (lots of) debugging info (parsing, drawing etc.)
												in Gamemaker's log.
	
	------------
	 KNOWN BUGS
	------------
	
	
	
************************************************************************************************************************/
#endregion



function type_formatted(_x, _y, _string/*, _draw, _start_character, _speed*/) {
	
	var _actually_draw = true;
	if (argument_count > 3) {
		var _actually_draw = argument[3];	
	}
	var _start_character = 0;
	if (argument_count > 4) {
		var _start_character = argument[4];
	}
	var _typewriter_delay = -1;
	if (argument_count > 5) {
		_typewriter_delay = argument[5];
	}
	
	
	// Parse string if not already parsed
	var _struct = fnc_TypeFormatted_FindParsedString(_string);
	if (_struct == undefined) {		
		type_formatted_create(_x, _y, _string, _start_character, _typewriter_delay);
		_struct = fnc_TypeFormatted_FindParsedString(_string);
	}
	
	// Render string
	if (_actually_draw) {
		type_formatted_draw(_x, _y, _string);
	}
	
	// Return result struct
	return _struct;
}

function type_formatted_draw(_x, _y, _string) {
	
	// ======================
	// Perform final draw
	// ======================
	var _curr_scale_x = TYPE_FORMATTED_DEFAULT_XSCALE;
	var _curr_scale_y = TYPE_FORMATTED_DEFAULT_YSCALE;
	var _curr_angle = 0;
	var _curr_blend = c_white;
	var _curr_sep = -1;
	var _curr_wrap = -1;
	
	// Reset font if needed
	fnc_draw_set_font(TYPE_FORMATTED_DEFAULT_FONT);
	fnc_draw_set_alpha(TYPE_FORMATTED_DEFAULT_ALPHA);
	
	var _structString = fnc_TypeFormatted_FindParsedString(_string);
		
	if (_structString.halign == fa_center) {
		var _curr_x = _x - _structString.bbox_width/2;
	}
	else if (_structString.halign == fa_right) {
		var _curr_x = _x - _structString.bbox_width;
	}
	else {
		var _curr_x = _x;
	}
		
	draw_set_halign(fa_left);
	draw_set_valign(_structString.valign);
	
	if (TYPE_FORMATTED_DEBUG_NOTES) {
		show_debug_message("=== [type_formatted/type_formatted_draw]: NOTE: Drawing parsed item from cache: "+_string);
	}
			
	// Draw the elements corresponding to the currently parsed string
	var _n = array_length(_structString.parsed_string_array);
	var _s = 0; // index for sprite array
	var _i = 0; // index for element arays
	var _character_limit = _structString.typewriter_delay == -1 ? 99999999 : _structString.current_character;
	var _count = 0;
	var _next_item = true;
	
	while (_next_item && _i<_n && _count < _character_limit) {
		var _parameter_array = _structString.parameter_array[_i];
		var _element = _structString.parsed_string_array[_i];
		if (string_copy(_element, 1, 1) == "@") { // Process control codes for sprites, etc.
			if (string_copy(_element, 1, 7) == "@sprite") {
				var _sprite_index = _structString.sprite_array[_s];
				var _initial_subimg_array = _structString.current_subimg[_s];
				
				_curr_scale_x = real(_parameter_array[0]);
				_curr_scale_y = real(_parameter_array[1]);
				_curr_angle = real(_parameter_array[2]);
				//_curr_blend = fnc_TypeFormatted_Color(_parameter_array[3]);
				_curr_blend = fnc_parse_color(string_replace_all(_parameter_array[3], " ", ""));
				_curr_sep = real(_parameter_array[4]);
				_curr_wrap = real(_parameter_array[5]);
		
				// Determine offsets for non top-left sprites
				var _offset_x = sprite_get_xoffset(_sprite_index) * _curr_scale_x;
				var _offset_y = sprite_get_yoffset(_sprite_index) * _curr_scale_y;				
				
				
				// Determine alignment for sprites
				if (draw_get_valign() == fa_top) {							
					var _y1sprite = _y + _offset_y;						
				}
				else if (draw_get_valign() == fa_bottom) {
					var _y1sprite = _y + _offset_y - sprite_get_height(_sprite_index)*_curr_scale_y;
				}
				else {
					var _y1sprite = _y + _offset_y - sprite_get_height(_sprite_index)*_curr_scale_y/2;
				}
				
				draw_sprite_ext(_sprite_index, _initial_subimg_array, _curr_x + _offset_x, _y1sprite, _curr_scale_x, _curr_scale_y, _curr_angle, _curr_blend, draw_get_alpha());
				_curr_x = _curr_x + sprite_get_width(_sprite_index) * _curr_scale_x;			
				_s++;
				_count++;
				_next_item = true;
			}
			else { // Process control codes for draw alpha/font/etc.
								
				var _function = string_copy(_element, 2, string_length(_element));
				switch (_function) {
					case "draw_set_alpha":
						fnc_draw_set_alpha(_parameter_array);
						break;
					case "draw_set_color":
						fnc_draw_set_color(_parameter_array);
						break;
					case "draw_set_font": 
						fnc_draw_set_font(_parameter_array);
						break;
					default:
						show_debug_message("Warning, "+_function+" called and the switch statement that processes the formatting changes did not account for it!");
						break;
				}
				
				
				_next_item = true;
			}
		}
		else { // Process draw text
			_curr_scale_x = real(_parameter_array[0]);
			_curr_scale_y = real(_parameter_array[1]);
			_curr_angle = real(_parameter_array[2]);
			_curr_blend = fnc_parse_color(string_replace_all(_parameter_array[3], " ", ""));
			_curr_sep = real(_parameter_array[4]);
			_curr_wrap = real(_parameter_array[5]);	
			
			
			var _max_string_chars = _character_limit - _count;
			var _string_length = string_length(_element);
			var _final_chars_to_consider = min(_max_string_chars, _string_length);
			
			var _final_string = string_copy(_element, 1, _final_chars_to_consider);
			
			//var _function_call = asset_get_index("fnc_draw_text_ext_transformed");
			var _parameter_array = [_curr_x, _y, _final_string, _curr_sep, _curr_wrap, _curr_scale_x, _curr_scale_y, _curr_angle, _curr_blend];
			//_function_call(_parameter_array);
			fnc_draw_text_ext_transformed(_parameter_array);
			
			_curr_x = _curr_x + string_width(_element) * _curr_scale_x;		
			_count = _count + _final_chars_to_consider;
			_next_item = (_final_chars_to_consider == _string_length);
		}
		if (_next_item) {
			_i++;	
		}		
	}
	


	if (TYPE_FORMATTED_DEBUG_VERBOSE) {
		show_debug_message("=== [type_formatted/type_formatted_draw]: Finalized drawing "+_string);
	}
	
	
	
	
	
	// Debug bbox
	if (TYPE_FORMATTED_DEBUG_BBOX) {
		draw_set_halign(fa_center);
		draw_set_valign(fa_bottom);
		draw_set_font(font_exists(fnt_Debug) ? fnt_Debug : -1);
		draw_set_color(c_black);
		draw_set_alpha(1);
				
		var _bbox_coords = _structString.bbox(_x,_y);
				
		draw_circle_color(_bbox_coords[0], _bbox_coords[1], 2, c_green, c_green, false);
		draw_text(_bbox_coords[0], _bbox_coords[1], string(_bbox_coords[0])+","+string(_bbox_coords[1]));
		draw_circle_color(_bbox_coords[0], _bbox_coords[3], 2, c_dkgray, c_dkgray, false);
		draw_text(_bbox_coords[0], _bbox_coords[3]+20, string(_bbox_coords[0])+","+string(_bbox_coords[3]));
		draw_line_color(_bbox_coords[0], _bbox_coords[1], _bbox_coords[2], _bbox_coords[1], c_gray, c_gray);
		draw_circle_color(_bbox_coords[2], _bbox_coords[1], 2, c_dkgray, c_dkgray, false);
		draw_text(_bbox_coords[2], _bbox_coords[1], string(_bbox_coords[2])+","+string(_bbox_coords[1]));
		draw_circle_color(_bbox_coords[2], _bbox_coords[3], 2, c_blue, c_blue, false);
		draw_text(_bbox_coords[2], _bbox_coords[3]+20, string(_bbox_coords[2])+","+string(_bbox_coords[3]));
		draw_line_color(_bbox_coords[0], _bbox_coords[3], _bbox_coords[2], _bbox_coords[3], c_gray, c_gray);
		draw_circle_color(_x, _y, 7, c_red, c_red, false);
		draw_set_alpha(0.2);
		draw_set_color(c_blue);
		draw_rectangle(_bbox_coords[0], _bbox_coords[1], _bbox_coords[2], _bbox_coords[3], false);
		if (TYPE_FORMATTED_DEBUG_VERBOSE) {
			show_debug_message("=== [type_formatted/type_formatted_draw]: NOTE: Anchor point (provided x,y): "+string(_x)+","+string(_y)+"; bbox coordinates: "+string(_bbox_coords[0])+","+string(_bbox_coords[1])+" "+string(_bbox_coords[2])+","+string(_bbox_coords[1])+" "+string(_bbox_coords[0])+","+string(_bbox_coords[3])+" "+string(_bbox_coords[2])+","+string(_bbox_coords[3]));
		}
	}
}

function type_formatted_create(_x, _y, _string /* _start_character, _speed */) {
	
	var _initial_character = 0; // All characters by default
	if (argument_count > 3) {
		_initial_character = argument[3];
	}
	var _typewriter_delay = -1; // No animation by default
	if (argument_count > 4) {
		_typewriter_delay = argument[4];
	}
	
	if (TYPE_FORMATTED_DEBUG_NOTES) {		
		show_debug_message("=== [type_formatted/type_formatted_create]: NOTE: Parsing string: "+_string);
	}

	
	var _inside_text_split = noone;
	
	try {		
		if (string_count("[",_string)-string_count("\\[",_string) != string_count("]",_string)-string_count("\\]",_string)) {
			throw("=== [type_formatted/type_formatted_create]: ERROR: tag open/close mismatch");
		}
		else {
			// TODO: Check for nested []
			// TODO: Improve data validation
			// TODO: Factor rotation into width/height
			
			var _n = string_length(_string);
			
			var _inside = false;
			var _inside_text = "";
			var _text = "";
			var _escape = false;
			
			var _parsed_array = [];
			var _sprite_array = [];
			
			var _initial_subimg_array = [];
			var _speed_array = [];
			var _parameter_array = [];
			
			var _curr_width = 0;
			var _curr_height = 0;
			
			// Scales and angles, apply to sprites and text
			var _curr_scale_x = 1.0;
			var _curr_scale_y = 1.0;
			var _curr_angle = 0.0;
			var _curr_blend = "c_white";
			var _curr_sep = -1;
			var _curr_wrap = -1;
			
			var _curr_halign = fa_left;
			var _curr_valign = fa_top;
			
			fnc_draw_set_font(TYPE_FORMATTED_DEFAULT_FONT);
			
			// Process string
			for (var _i=1; _i<=_n; _i++) {
				var _chr = string_copy(_string, _i, 1);
				if (_chr == "\\" && !_escape) { // Enable escape character
					_escape = true;
				}
				else if (_escape) { // Process escaped character as if it is text					
					_text = _text + _chr;
					_curr_width = _curr_width + string_width(_chr)*_curr_scale_x;
					_curr_height = max(_curr_height, string_height(_chr)*_curr_scale_y);
					
					_escape = false;
				}
				else if (_chr == "[") { // Go inside for a tag					
					_inside = true;	
					
					// Add current text if not already there and not empty					
					if (_text != "") {	
						array_push(_parsed_array, _text);
						array_push(_parameter_array, [_curr_scale_x, _curr_scale_y, _curr_angle, _curr_blend, _curr_sep, _curr_wrap]);
					}
					_text = "";
				}
				else if (_chr == "]") { // Process inside tag, keep only the first part (ignore arguments after commas)
					_inside = false;
					var _full_inside_text = string_replace_all(_inside_text, " ", ""); //fnc_String_lrtrim(_inside_text);
					var _inside_text_split = fnc_StringToList(_full_inside_text, ",", true);
					var _n_split = ds_list_size(_inside_text_split);
					
					_inside_text = _inside_text_split[|0];
					// See if first element of inside text tag matches an asset
					var _idx = asset_get_index(_inside_text);
					var _idxtype = asset_get_type(_inside_text);
					
					if (TYPE_FORMATTED_DEBUG_VERBOSE) {
						show_debug_message("=== [type_formatted/type_formatted_create]: Trying to find assets for tag ["+_inside_text+"] with index "+string(_idx)+" and type "+string(_idxtype));
					}
					
					if (string_length(_inside_text) == 0) { // Reset default alpha, color, scalex, scaley and angle - NOTE, no alignment reset						
						// Reset scaling
						_curr_scale_x = 1.0;
						_curr_scale_y = 1.0;
						_curr_angle = 0.0;
						_curr_blend = "c_white";
						_curr_wrap = -1;
						_curr_sep = -1;
						
						// Add font reset
						array_push(_parsed_array, "@TypeFormatted_Defaults");
						array_push(_parameter_array, []);
						draw_set_font(TYPE_FORMATTED_DEFAULT_FONT); // needed for bbox
					}
					else if (string_copy(_inside_text, 1, 3) == "chr") { // include ASCII character
						var _txtcode = chr(real(_inside_text_split[|1]));
						_curr_width = _curr_width + string_width(_txtcode)*_curr_scale_x;
						_curr_height = max(_curr_height, string_height(_txtcode)*_curr_scale_y);
						array_push(_parsed_array, _txtcode);
						array_push(_parameter_array, [_curr_scale_x, _curr_scale_y, _curr_angle, _curr_blend, _curr_sep, _curr_wrap]);					
					
					}					
					else if (string_copy(_inside_text, 1, 1) == "#" || string_copy(_inside_text, 1, 1) == "$") { // Change color, custom
						array_push(_parsed_array, "@draw_set_color");
						array_push(_parameter_array, [_inside_text]);						
					}
					else if (string_copy(_inside_text, 1, 8) == "c_random") {
						if (TYPE_FORMATTED_DEBUG_VERBOSE) {
							show_debug_message("random parsed");
						}
						array_push(_parsed_array, "@draw_set_color");
						array_push(_parameter_array, [_inside_text]);	
					}
					else if (string_copy(_inside_text, 1, 2) == "c_") { // Change color, built-in constant
						if (!is_undefined(ds_map_find_value(global.tf_colors, _inside_text))) {
							array_push(_parsed_array, "@draw_set_color");
							array_push(_parameter_array, [_inside_text]);	
						}						
						else {
							if (TYPE_FORMATTED_DEBUG_VERBOSE) {
								show_debug_message("=== [type_formatted/type_formatted_create]: WARNING: ignored '"+_inside_text+"', no such color constant exists");	
							}
						}
					}
					else if (string_copy(_inside_text, 1, 5) == "angle") { // Change angle ***
						if (_n_split != 2) {
							if (TYPE_FORMATTED_DEBUG_VERBOSE) {
								show_debug_message("=== [type_formatted/type_formatted_create]: WARNING: ignored '"+_inside_text+"', no valid value specified for angle");	
							}
						}
						else {
							_curr_angle = _inside_text_split[|1] % 360;
						}						
					}
					else if (string_copy(_inside_text, 1, 5) == "blend") { // Change blend color ***
						if (_n_split != 2) {
							if (TYPE_FORMATTED_DEBUG_VERBOSE) {
								show_debug_message("=== [type_formatted/type_formatted_create]: WARNING: ignored '"+_inside_text+"', no valid value specified for blend");	
							}
						}
						else {
							_curr_blend = _inside_text_split[|1];
						}						
					}
					else if (string_copy(_inside_text, 1, 3) == "sep") { // Change sep ***
						if (_n_split != 2) {
							if (TYPE_FORMATTED_DEBUG_VERBOSE) {
								show_debug_message("=== [type_formatted/type_formatted_create]: WARNING: ignored '"+_inside_text+"', no valid value specified for sep");	
							}
						}
						else {
							_curr_sep = _inside_text_split[|1];
						}						
					}
					else if (string_copy(_inside_text, 1, 4) == "wrap") { // Change wrap ***
						if (_n_split != 2) {
							if (TYPE_FORMATTED_DEBUG_VERBOSE) {
								show_debug_message("=== [type_formatted/type_formatted_create]: WARNING: ignored '"+_inside_text+"', no valid value specified for wrap");	
							}
						}
						else {
							_curr_wrap = _inside_text_split[|1];
						}						
					}
					else if (string_copy(_inside_text, 1, 5) == "alpha") { // Change alpha
						if (_n_split != 2) {
							if (TYPE_FORMATTED_DEBUG_VERBOSE) {
								show_debug_message("=== [type_formatted/type_formatted_create]: WARNING: ignored '"+_inside_text+"', no valid value specified for alpha");	
							}
						}
						else {
							array_push(_parsed_array, "@draw_set_alpha");
							array_push(_parameter_array, [clamp(real(_inside_text_split[|1]), 0, 1)]);
						}
					}
					else if (string_copy(_inside_text, 1, 5) == "scale") { // Change scaling
						if (_n_split < 2 || _n_split > 3) {
							if (TYPE_FORMATTED_DEBUG_VERBOSE) {
								show_debug_message("=== [type_formatted/type_formatted_create]: WARNING: ignored '"+_inside_text+"', no valid values for scaling");	
							}
						}
						else {
							_curr_scale_x = real(_inside_text_split[|1]);
							if (_n_split == 2) {
								_curr_scale_y = _curr_scale_x;
							}
							else {
								_curr_scale_y = real(_inside_text_split[|2]);
							}
						}
					}					
					else if (string_copy(_inside_text, 1, 3) == "fa_") { // Change alignment
						if (_inside_text == "fa_left" || _inside_text == "fa_center" || _inside_text == "fa_right") {
							if (!is_undefined(ds_map_find_value(global.tf_halignments, _inside_text))) {
								_curr_halign = ds_map_find_value(global.tf_halignments, _inside_text);
							}
						}
						else if (_inside_text == "fa_top" || _inside_text == "fa_middle" || _inside_text == "fa_bottom") {
							if (!is_undefined(ds_map_find_value(global.tf_valignments, _inside_text))) {								
								_curr_valign = ds_map_find_value(global.tf_valignments, _inside_text);
							}
						}
						else {
							if (TYPE_FORMATTED_DEBUG_VERBOSE) {
								show_debug_message("=== [type_formatted/type_formatted_create]: WARNING: ignored '"+_inside_text+"', no such alignment value exists");	
							}
						}							
					}
					else if (_idx != -1) {
						if (TYPE_FORMATTED_DEBUG_VERBOSE) {
							show_debug_message("=== [type_formatted/type_formatted_create]: NOTE: Confirmed, ["+_inside_text+"] is an asset of type "+string(_idxtype));
						}
						
						if (_idxtype == asset_sprite) {							
							
							if (sprite_exists(_idx)) { // Generate sprite element
								if (_n_split > 3) {
									if (TYPE_FORMATTED_DEBUG_VERBOSE) {
										show_debug_message("=== [type_formatted/type_formatted_create]: WARNING: ignored '"+_inside_text+"', invalid parameters");	
									}
								}
								else {									
									if (_n_split == 1) {
										var _subimg = 0;
										var _animation_speed = 0;
									}
									else if (_n_split == 2) {
										var _subimg = real(_inside_text_split[|1]);
										var _animation_speed = 0;
									}
									else if (_n_split == 3) {
										var _subimg = real(_inside_text_split[|1]);
										if (string_length(_inside_text_split[|2]) == 0) {
											var _animation_speed = sprite_get_speed(_idx);
										}
										else {
											var _animation_speed = real(_inside_text_split[|2]);	
										}										
									
									}
									var _sprite_width = sprite_get_width(_idx)*_curr_scale_x;
									var _sprite_height = sprite_get_height(_idx)*_curr_scale_y;
									_curr_width = _curr_width + _sprite_width;
									_curr_height = max(_curr_height, _sprite_height);
								
									// Add sprite arrays
									array_push(_parsed_array, "@sprite");
									array_push(_sprite_array, _idx);									
									array_push(_initial_subimg_array, _subimg);
									array_push(_speed_array, _animation_speed);
									array_push(_parameter_array, [_curr_scale_x, _curr_scale_y, _curr_angle, _curr_blend, _curr_sep, _curr_wrap]);
								}
								
							}
						}
						else if (_idxtype == asset_font) {							
							if (font_exists(_idx)) { // Change font ***
								array_push(_parsed_array, "@draw_set_font");
								array_push(_parameter_array, [_idx]);
								draw_set_font(_idx); // needed for bbox
							}
						}						
						else {
							if (TYPE_FORMATTED_DEBUG_VERBOSE) {
								show_debug_message("=== [type_formatted/type_formatted_create]: WARNING: ignored '"+_inside_text+"', no such sprite or font exists");
							}
						}
					}
					else {
						if (TYPE_FORMATTED_DEBUG_VERBOSE) {
							show_debug_message("=== [type_formatted/type_formatted_create]: WARNING: ignored '"+_inside_text+"', no such asset exists and no valid keyword was found");
						}
					}
					_inside_text = "";
				}
				else if (_inside) { // Save inside tag into a string variable
					_inside_text = _inside_text + _chr;
				}
				else { // Save text into a variable, write text if it's the last character of the string					
					_text = _text + _chr;
					_curr_width = _curr_width + string_width(_chr)*_curr_scale_x;
					_curr_height = max(_curr_height, string_height(_chr)*_curr_scale_y);
					
					// Add last text element
					
					if (_i==_n) {						
						if (_text != "") {								
							// Add current text if not already there and not empty										
							array_push(_parsed_array, _text);
							array_push(_parameter_array, [_curr_scale_x, _curr_scale_y, _curr_angle, _curr_blend, _curr_sep, _curr_wrap]);
						}
						_text = "";						
					}
				}
			}
			
			
			
			// Clean
			if (ds_exists(_inside_text_split, ds_type_list)) {
				ds_list_destroy(_inside_text_split);
			}
			
			
			
		}
	}

	var _result = new global.TypeFormatted_ParsedElement(_parsed_array, _sprite_array, _initial_subimg_array, _speed_array, _parameter_array, _curr_width, _curr_height, _curr_halign, _curr_valign, _initial_character, _typewriter_delay);
	ds_map_add(	global.TypeFormatted_parsedElementMap,
				_string, 
				_result);
				
	if (TYPE_FORMATTED_DEBUG_NOTES) {
		show_debug_message("=== [type_formatted/type_formatted_create]: Parsed "+_string + " / Total width="+string(_curr_width)+" *** Total height "+string(_curr_height));
	}

	
}

function fnc_TypeFormatted_FindParsedString(_string_id) {
	return global.TypeFormatted_parsedElementMap[? _string_id];
}

function fnc_TypeFormatted_Defaults() {
	// No alignment reset
	// Scale, angle, blend and wrap are reset at the corresponding step
	if (draw_get_font() != TYPE_FORMATTED_DEFAULT_FONT) {
		draw_set_font(TYPE_FORMATTED_DEFAULT_FONT);
	}
	if (draw_get_color() != TYPE_FORMATTED_DEFAULT_COLOR) {
		draw_set_color(TYPE_FORMATTED_DEFAULT_COLOR);
	}
	if (draw_get_alpha() != TYPE_FORMATTED_DEFAULT_ALPHA) {
		draw_set_alpha(TYPE_FORMATTED_DEFAULT_ALPHA);
	}
}

function fnc_TypeFormatted_Flush() {
	if (ds_exists(global.TypeFormatted_parsedElementMap, ds_type_map)) {
		ds_map_destroy(global.TypeFormatted_parsedElementMap);		
		global.TypeFormatted_parsedElementMap = ds_map_create();
		if (TYPE_FORMATTED_DEBUG_VERBOSE) {
			show_debug_message("NOTE: Flushed TypeFormatted cache");
		}
	}	
}


function fnc_TypeFormatted_CacheSize() {
	if (ds_exists(global.TypeFormatted_parsedElementMap, ds_type_map)) {
		return ds_map_size(global.TypeFormatted_parsedElementMap);
	}
	else {
		return -1;
	}	
}