
#region Menu
	
	/// @function fnc_DrawMenu
	/// @description Draw the menu to the screen and perform mouseover/click handlers
	function fnc_DrawMenu() {
		
		var _title_color = "[c_green]";
		var _main_text_color = "[c_white]";
		var _link_color = "[c_white]";
		var _link_hover_color = "[c_yellow]";
		
		var _w = GUI_WIDTH;
		var _h = GUI_HEIGHT;
		
		var _y_title = 60;
		
		type_formatted(_w/2, _y_title, "[fnt_Title][fa_middle][fa_center][scale,1.2]"+_title_color+game_title);
		
		var _n = array_length(menu_items);
		var _startY = _y_title + 100;
		var _spacing = 40;
		for (var _i = 0; _i<_n; _i++) {	
			fnc_Link(_w/2, _startY + _i*_spacing, "[fa_middle][fa_center]"+_link_color+menu_items[_i], "[fa_middle][fa_center]"+_link_hover_color+menu_items[_i], fnc_ExecuteMenu, _i);
		}
		
		type_formatted(_w/2, _h-100, "[fnt_Title][fa_middle][fa_center][scale,0.8]A game by José Bonilla for [spr_LudumDare] 48");
			
	}


	function fnc_Menu_0 () {
		transition_id = instance_create_layer(0, 0, layer_get_id("lyr_Instances"), cls_Transition);
		with  (transition_id) {
			destination_room = room_Game_1;
			transition_type = TRANSITION.SQUARES;
			max_time = 60;
			event_perform(ev_other, ev_user0);
		}
	}
	
	function fnc_Menu_1 () {
		room_goto(room_UI_HowToPlay);
	}


	function fnc_Menu_2 () {
		room_goto(room_UI_Options);
	}

	
	function fnc_Menu_3 () {
		room_goto(room_UI_Credits);
	}

	function fnc_Menu_4 () {
		game_end();
	}


#endregion

#region Options

	/// @function fnc_DrawOptions
	/// @description Draw the options to screen and perform mouseover/click handlers

	function fnc_DrawOptions() {
		var _w = GUI_WIDTH;
		var _h = GUI_HEIGHT;

		var _title_color = "[c_green]";
		var _main_text_color = "[c_white]";
		var _link_color = "[c_white]";
		var _link_hover_color = "[c_yellow]";
	
		var _slider_color = $fffdff;
		var _slider_handle_color = $fffdff;
		var _slider_handle_drag_color = $e6ff0b;
				
		var _y_title = 60;
		type_formatted( _w/2, _y_title, "[fa_middle][fa_center][fnt_Title]"+_title_color+"Options");
		var _startY = _y_title+100;
		var _spacing = 40;
		
		var _n = array_length(option_items);

		for (var _i=0; _i<_n; _i++) {
			if (option_type[? option_items[_i]] == "checkbox" || option_type[? option_items[_i]] == "toggle") {
				var _sprite = asset_get_index("spr_"+string_upper(string_copy(option_type[? option_items[_i]], 1, 1))+string_copy(option_type[? option_items[_i]],2,string_length(option_type[? option_items[_i]])));
				draw_sprite_ext(_sprite, option_value[? option_items[_i]], _w/2-100, _startY+_i*_spacing, 1, 1, 0, c_white, 1);
				var _mousex = device_mouse_x_to_gui(0);
				var _mousey = device_mouse_y_to_gui(0);	
				var _mouseover = _mousex >= _w/2-100 - sprite_get_width(_sprite)/2 && _mousex <= _w/2-100 + sprite_get_width(_sprite)/2 && _mousey >= _startY+_i*_spacing - sprite_get_height(_sprite)/2 && _mousey <= _startY+_i*_spacing+sprite_get_height(_sprite)/2;
				if (device_mouse_check_button_pressed(0, mb_left) && _mouseover) {
					fnc_ExecuteOptions(_i);
				}
				fnc_Link(_w/2-100+sprite_get_width(_sprite), _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_color+option_items[_i], "[fa_middle][fa_left]"+_link_hover_color+option_items[_i], fnc_ExecuteOptions, _i);
			}
			else if (option_type[? option_items[_i]] == "slider") {
				fnc_Link(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_color+option_items[_i], "[fa_middle][fa_left]"+_link_hover_color+option_items[_i], fnc_ExecuteOptions, _i);
				var _temp_struct = type_formatted(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_color+option_items[_i], false);
				
				var _slider_start = _w/2-100 + _temp_struct.bbox_width + 20;
				var _slider_end = _slider_start+60+20;
				var _mousex = device_mouse_x_to_gui(0);
				var _mousey = device_mouse_y_to_gui(0);
				var _handle_radius = 9;
				var _mouseover_circle = point_in_circle(_mousex, _mousey, _slider_start + (_slider_end-_slider_start) * option_value[? option_items[_i]], _startY+_i*_spacing+6/3, _handle_radius);
				
				// Draw slider
				draw_rectangle_color(_slider_start, _startY+_i*_spacing-3, _slider_end, _startY+_i*_spacing+3, _slider_color, _slider_color, _slider_color, _slider_color, false);
				
				// Draw handle
				if (_mouseover_circle || start_drag_drop) {
					var _color_circle = _slider_handle_drag_color;
				}
				else {
					var _color_circle = _slider_handle_color;
				}
				draw_circle_color(_slider_start + (_slider_end-_slider_start) * option_value[? option_items[_i]], _startY+_i*_spacing, _handle_radius, _color_circle, _color_circle, false);
				
				// Handle drag & drop				
				if (device_mouse_check_button(0, mb_left) && (_mouseover_circle || start_drag_drop)) {							
					option_value[? option_items[_i]] = (clamp(_mousex, _slider_start, _slider_end) - _slider_start) / (_slider_end - _slider_start);					
					start_drag_drop = true;
				}
				else {
					start_drag_drop = false;
				}
				
				// Display %
				if (start_drag_drop) {					
					type_formatted(_slider_start + (_slider_end-_slider_start) * option_value[? option_items[_i]], _startY+_i*_spacing-30, "[fa_center]"+_link_color+string(round(option_value[? option_items[_i]]*100))+"%");
				}
				
			}
			else if (option_type[? option_items[_i]] == "input") {	
				if (name_being_modified) {
					fnc_Link(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_hover_color+option_items[_i]+": [fnt_MiniText] "+option_value[? option_items[_i]], "[fa_middle][fa_left]"+_link_hover_color+option_items[_i]+": [fnt_MiniText] "+_link_hover_color+option_value[? option_items[_i]], noone, 0);
					if (keyboard_lastkey == vk_enter) { // finalize
						option_value[? option_items[_i]] = keyboard_string == "" ? "Player" : string_copy(keyboard_string,1,16);
						name_being_modified = false;
					}
					else {
						keyboard_string = string_copy(keyboard_string,1,16);
						option_value[? option_items[_i]] = keyboard_string;
					}
				}
				else {					
					fnc_Link(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_color+option_items[_i]+": [fnt_MiniText] "+option_value[? option_items[_i]], "[fa_middle][fa_left]"+_link_hover_color+option_items[_i]+": [fnt_MiniText] "+option_value[? option_items[_i]], fnc_ExecuteOptions, _i);
				}
			}
			else { // Regular link
				fnc_Link(_w/2-120, _startY+_i*_spacing, "[fa_middle][fa_left]"+_link_color+option_items[_i], "[fa_middle][fa_left][c_green]"+_link_hover_color+option_items[_i], fnc_ExecuteOptions, _i);	
			}
		}
		fnc_Link(_w/2, _h-60, "[fa_middle][fa_center]"+_link_color+"Return to Main Menu", "[fa_middle][fa_center]"+_link_hover_color+"Return to Main Menu", fnc_ReturnToMainMenu, 0);	
	}


	/// @function fnc_Options_0
	/// @description Perform the click of the corresponding option 

	function fnc_Options_0() {
		fnc_Options_Checkbox(0);
	}


	/// @function fnc_Options_1
	/// @description Perform the click of the corresponding option 

	function fnc_Options_1() {
		fnc_Options_Checkbox(1);
	}


	/// @function fnc_Options_2
	/// @description Perform the click of the corresponding option 

	function fnc_Options_2() {
		fnc_Options_Checkbox(2);
	}

	/// @function fnc_Options_3
	/// @description Perform the click of the corresponding option 

	function fnc_Options_3() {
		fnc_Options_Checkbox(3);
		fullscreen_change = true;
	}

	/// @function fnc_Options_4
	/// @description Perform the click of the corresponding option 

	function fnc_Options_4() {
		room_goto(room_UI_Options_Controls);
	}
	
	/// @function fnc_Options_5
	/// @description Perform the click of the corresponding option 

	function fnc_Options_5() {
		name_being_modified = true;
		keyboard_string = option_value[? option_items[5]];
	}
	
	function fnc_DrawOptionsControls() {
		var _w = GUI_WIDTH;
		var _h = GUI_HEIGHT;
		
		var _title_color = "[c_green]";
		var _main_text_color = "[c_white]";
		var _link_color = "[c_white]";
		var _link_hover_color = "[c_yellow]";
		
		var _y_title = 60;
		type_formatted(_w/2, _y_title, "[fa_middle][fa_center][fnt_Title]"+_title_color+"Controls");
		
		var _n = ds_map_size(controls);
		for (var _i=0; _i<_n; _i++) {
			if (wait_for_input && key_being_remapped == control_indices[_i]) {
				type_formatted(_w/2, _h-30, _link_color+"[fa_middle][fa_center][fnt_Menu]PRESS ANY KEY TO REMAP");
				fnc_Link(_w/2-150, _y_title+60+_i*30, "[fa_middle][fa_left][fnt_MiniText]"+_link_hover_color+control_names[? control_indices[_i]]+": ", "[fa_middle][fa_left][fnt_MiniText]"+_link_hover_color+control_names[? control_indices[_i]]+": ", noone, 0);
				fnc_AssignControls(control_indices[_i]);
			}
			else {
				//show_debug_message(control_names[? control_indices[_i]]);
				fnc_Link(_w/2-150, _y_title+60+_i*30, "[fa_middle][fa_left][fnt_MiniText]"+_link_color+control_names[? control_indices[_i]]+": "+fnc_KeyToString(controls[? control_indices[_i]]), "[fa_middle][fa_left][fnt_MiniText]"+_link_hover_color+control_names[? control_indices[_i]]+": "+fnc_KeyToString(controls[? control_indices[_i]]), fnc_AssignControls, control_indices[_i]);
			}
		}
		
		
		fnc_Link(_w/2, _h-60, "[fa_middle][fa_center]"+_link_color+"Return to Options", "[fa_middle][fa_center]"+_link_hover_color+"Return to Options", fnc_Menu_2, 0);
	}
	
	function fnc_AssignControls(_key) {			
		if (wait_for_input) {
			if (key_being_remapped == noone) {
				keyboard_lastkey = noone;
				key_being_remapped = _key;
			}
			else if (keyboard_lastkey != noone) {
				if (keyboard_lastkey != vk_escape) {
					controls[? _key] = keyboard_lastkey;					
				}
				key_being_remapped = noone;
				wait_for_input = false;
			}			
		}
		else {
			wait_for_input = true;
		}
	}
	
	/// @function fnc_ReturnToMainMenu
	/// @description Return to Main Menu

	function fnc_ReturnToMainMenu() {
		room_goto(room_UI_Title);
	}
	

	/// @function fnc_Options_Checkbox
	/// @description Auxiliary function to enable/disable checkbox

	function fnc_Options_Checkbox(_i) {
		option_value[? option_items[_i]] = !option_value[? option_items[_i]];
	}

#endregion	

#region Credits

function fnc_DrawCredits() {
	var _w = GUI_WIDTH;
	var _h = GUI_HEIGHT;
	
	var _title_color = "[c_green]";
	var _main_text_color = "[c_white]";
	var _link_color = "[c_white]";
	var _link_hover_color = "[c_yellow]";
	
	var _y_title = 60;
	type_formatted(_w/2, _y_title, _title_color+"[fnt_Title][fa_middle][fa_center]Credits");
	var _startY = _y_title+120;
	var _spacing = 30;
		
	for (var _i=0; _i<array_length(credits); _i++) {
		type_formatted(_w/2, _startY + _i*_spacing, credits[_i]);
	}	
	
	fnc_Link(_w/2, _h-60, _link_color+"[fa_middle][fa_center]Return to Main Menu", _link_hover_color+"[fa_middle][fa_center][$0BFFE6]Return to Main Menu", fnc_ReturnToMainMenu, 0);	
}

#endregion

#region Instructions

function fnc_DrawHowToPlay() {
	var _w = GUI_WIDTH;
	var _h = GUI_HEIGHT;
	
	var _title_color = "[c_green]";
	var _main_text_color = "[c_white]";
	var _link_color = "[c_white]";
	var _link_hover_color = "[c_yellow]";
	
	var _y_title = 60;
	type_formatted(_w/2, _y_title, _title_color+"[fnt_Title][fa_middle][fa_center]Help");
	var _startY = _y_title+80;
	var _spacing = 30;
		
	// Draw text...
	type_formatted(_w/2, _startY, _main_text_color+"[fnt_MiniText][fa_center][fa_middle]You are [spr_Player]");
	
	fnc_Link(_w/2, _h-60, _link_color+"[fa_middle][fa_center]Return to Main Menu", _link_hover_color+"[fa_middle][fa_center][$0BFFE6]Return to Main Menu", fnc_ReturnToMainMenu, 0);	
}

#endregion

#region HUD/Lost/Won/Pause Menu

function fnc_DrawPauseMenu() {
	var _w = GUI_WIDTH;
	var _h = GUI_HEIGHT;
	
	var _title_color = "[c_green]";
	var _main_text_color = "[c_white]";
	var _link_color = "[c_white]";
	var _link_hover_color = "[c_yellow]";
	
	if (sprite_exists(pause_screenshot)) {
		draw_sprite_ext(pause_screenshot, 0, 0, 0, 1, 1, 0, c_white, 1);
	}
	draw_set_alpha(0.7);	
	draw_rectangle_color(0, 0, _w, _h, c_black, $111111, $121212, $222222, false);
	type_formatted(_w/2, 30, _title_color+"[fa_center][fnt_Menu]Game Paused");
	type_formatted(_w/2, 60, _main_text_color+"[fa_center][fnt_Menu]Press ESC to resume");
	
	
	fnc_Link(_w/2, _h-60, _link_color+"[fa_center][fa_middle]Return to Main Menu", _link_hover_color+"[fa_center][fa_middle][$0BFFE6]Return to Main Menu", fnc_ReturnToMainMenu, 0);
}

function fnc_DrawYouLost() {
	var _w = GUI_WIDTH;
	var _h = GUI_HEIGHT;
	
	var _title_color = "[c_green]";
	var _main_text_color = "[c_white]";
	var _link_color = "[c_white]";
	var _link_hover_color = "[c_yellow]";
	
	draw_set_alpha(0.7);
	draw_set_color(c_black);
	draw_rectangle(0, 0, _w, _h, false);
	
	audio_stop_all();
	type_formatted(_w/2, 40, _title_color+"[fa_center][fnt_Menu][scale,2]+");
	if (death_message_chosen == "") {
		death_message_chosen = choose("Requiescat in Pace", "You died", "Goodbye cruel world", "You bit the dust", "You met your maker", "You passed away", "Boom.");
	}
	type_formatted(_w/2, 80, _main_text_color+"[fa_center][fnt_Menu]"+death_message_chosen);
	type_formatted(_w/2, 110, _link_hover_color+"[fa_center][fnt_Menu]Score: "+string(Game.total_score));
	
	if (ENABLE_SCOREBOARD) {
		// Scoreboard
		if (!scoreboard_queried) {
			//http_call = "query_scoreboard";
			var _scoreboard_url = "https://www.biyectivo.com/misc/scoreboard/scoreboard.php?game="+scoreboard_game_id+"&limit=5";
			http_get_id_query = http_get(_scoreboard_url);
			scoreboard_queried = true;
			if (current_scoreboard_updates < max_scoreboard_updates) {				
				Game.alarm[2] = timer_scoreboard_updates;
				current_scoreboard_updates++;
			}
		}
		else {
			if (http_return_status_query == 200) {
				
							
				var _list = ds_map_find_value(scoreboard_html5, "default");
				var _n = ds_list_size(_list);
				for (var _i=0; _i<_n; _i++) {
					var _map = ds_list_find_value(_list, _i);
					//show_debug_message(string(_i)+": "+string(_map[? "username"])+" = "+string(_map[? "game_score"]));
					draw_set_color(c_white);
						
					if (_i==0) {
						var _total_high_score = _map[? "game_score"];
					}
					else if (_i==4) {
						var _number_5_score	= _map[? "game_score"]; 
					}
					
					type_formatted(80, 400+_i*50, "[fa_right][scale,0.6]#"+string(_i+1));
					type_formatted(120, 400+_i*50, "[fa_left][scale,0.6]"+_map[? "username"]);
					type_formatted(500, 400+_i*50, "[fa_right][scale,0.6]"+_map[? "game_score"]);
				}
				
				
				/*particle_type_highscore =	part_type_create();		
				part_type_scale(particle_type_highscore, 1, 1);
				part_type_size(particle_type_highscore, 0.25, 0.35, 0, 0);
				part_type_life(particle_type_highscore, 5, 15);			
				part_type_shape(particle_type_highscore, pt_shape_star);
				
				part_type_alpha2(particle_type_highscore, 0.8, 0.0);
				part_type_speed(particle_type_highscore, 2, 5, 0, 0);
				part_type_direction(particle_type_highscore, 0, 360, 0, 20);
				part_type_orientation(particle_type_highscore, -10, 10, 0, 0, false);
				part_type_blend(particle_type_highscore, true);
		
		
				// High score indication
				if (Game.total_score >= _total_high_score) {
					var _txt = "YOU BEAT THE WORLD'S HIGH SCORE!!!";
					var _msgW = string_width(_txt);
					var _msgH = string_height(_txt);
					draw_text_transformed(_w/2, 340, _txt, 0.4, 0.4, 0);
					
					var _color = $00ccff;
					part_type_color1(particle_type_highscore, _color);
		
					// Emit
					part_emitter_region(Game.particle_system, Game.particle_emitter, _w/2-_msgW/2, _w/2+_msgW/2, 340-_msgH/2, 340+_msgH/2, ps_shape_ellipse, ps_distr_gaussian);
					part_emitter_burst(Game.particle_system, Game.particle_emitter, particle_type_highscore, 40);	
				}
				else if (Game.total_score >= _number_5_score) {
					var _txt = "YOU REACHED THE WORLD's TOP 5!!!"
					var _msgW = string_width(_txt);
					var _msgH = string_height(_txt);
					draw_text_transformed(_w/2+2, 340, _txt, 0.4, 0.4, 0);
					
					var _color = c_silver;					
					part_type_color1(particle_type_highscore, _color);
		
					// Emit
					part_emitter_region(Game.particle_system, Game.particle_emitter, _w/2-_msgW/2, _w/2+_msgW/2, 340-_msgH/2, 340+_msgH/2, ps_shape_ellipse, ps_distr_gaussian);
					part_emitter_burst(Game.particle_system, Game.particle_emitter, particle_type_highscore, 40);	
				}
				*/
				
			}
			else if (http_return_status_query == noone) {
				//draw_text_transformed(_w/2, 400, "Loading scoreboard...", 0.7, 0.7, 0);
				type_formatted(_w/2, 400, "[fa_center][fa_middle][scale,0.7]Loading scoreboard...");
			}
			/*
			else  {
				draw_set_halign(fa_center);
				draw_text_transformed(_w/2, 400, "No/bad connection", 0.7, 0.7, 0);				
				draw_text_transformed(_w/2, 440, "Cannot show scoreboard", 0.7, 0.7, 0);				
			}*/
		}
	}
	
	fnc_Link(_w/2, _h-120, _link_color+"[fa_center][fa_middle]ENTER to restart", _link_hover_color+"[fa_center][fa_middle][$0BFFE6]ENTER to restart", fnc_TryAgain, 0);
	fnc_Link(_w/2, _h-60, _link_color+"[fa_center][fa_middle]Return to Main Menu", _link_hover_color+"[fa_center][fa_middle][$0BFFE6]Return to Main Menu", fnc_ReturnToMainMenu, 0);
}

function fnc_DrawHUD() {
	if (ENABLE_LIVE && live_call()) return live_result;
	fnc_BackupDrawParams();
	// Draw the HUD
	var _w = GUI_WIDTH;
	var _h = GUI_HEIGHT;
	var _padding = 10;
	var _bgcolor = $222222;
	var _mgcolor = $111111;
	
	if (instance_exists(obj_TalkManager)) {
	}
	else if (minimize_GUI) {
		draw_set_alpha(0.85);
		draw_roundrect_color(_padding, _h-40-_padding, _w-_padding, _h-_padding, _bgcolor, _bgcolor, false);
		var _x1 = 3*_padding;
		var _y1 = _h-40+_padding;
		var _scale = 0.3;
		var _font_color = "[c_gray]";
		var _font_shadow_color = "[c_black]";
		type_formatted_shadow(_x1, _y1, "[fnt_Commands][fa_middle][fa_left][scale,"+string(_scale)+"]Press TAB to toggle GUI", _font_color, _font_shadow_color);
	}
	else {
		
		// Draw GUI Commands
		
		draw_set_alpha(0.85);	
		draw_roundrect_color(_padding, _h-140-_padding, _w-_padding, _h-_padding, _bgcolor, _bgcolor, false);
		draw_set_alpha(1);
		draw_roundrect_color(_padding, _h-140-_padding, _w-_padding, _h-_padding, _mgcolor, _mgcolor, true);
	
		var _x1 = 6*_padding;
		var _y1 = _h-140+_padding;
		var _scale = 0.3;
	
		var _color_selected = "[c_white]";
		var _color_mouseover = "[c_white]";
		var _color_normal = "[c_gray]";
		
		var _format_selected = "[fnt_Commands][fa_top][fa_center][scale,"+string(_scale)+"]";
		var _format_mouseover = "[fnt_Commands][fa_top][fa_center][scale,"+string(_scale)+"]";		
		var _format_normal = "[fnt_Commands][fa_top][fa_center][scale,"+string(_scale)+"]";
		
		fnc_Link(_x1, _y1,			(active_command == COMMANDS.TALK_TO ? _color_selected : _color_normal)+_format_selected+"Talk to",	_color_mouseover+_format_mouseover+"Talk to", fnc_ProcessCommands, COMMANDS.TALK_TO);
		fnc_Link(_x1, _y1+40,		(active_command == COMMANDS.PUSH ? _color_selected : _color_normal)+_format_normal+"Push",	_color_mouseover+_format_mouseover+"Push", fnc_ProcessCommands, COMMANDS.PUSH);
		fnc_Link(_x1, _y1+80,		(active_command == COMMANDS.PULL ? _color_selected : _color_normal)+_format_normal+"Pull",	_color_mouseover+_format_mouseover+"Pull", fnc_ProcessCommands, COMMANDS.PULL);
	
		fnc_Link(_x1+100, _y1,		(active_command == COMMANDS.USE ? _color_selected : _color_normal)+_format_normal+"Use",	_color_mouseover+_format_mouseover+"Use", fnc_ProcessCommands, COMMANDS.USE);
		fnc_Link(_x1+100, _y1+40,	(active_command == COMMANDS.OPEN ? _color_selected : _color_normal)+_format_normal+"Open",	_color_mouseover+_format_mouseover+"Open", fnc_ProcessCommands, COMMANDS.OPEN);
		fnc_Link(_x1+100, _y1+80,	(active_command == COMMANDS.CLOSE ? _color_selected : _color_normal)+_format_normal+"Close",	_color_mouseover+_format_mouseover+"Close", fnc_ProcessCommands, COMMANDS.CLOSE);
	
		fnc_Link(_x1+200, _y1,		(active_command == COMMANDS.PICK_UP ? _color_selected : _color_normal)+_format_normal+"Pick up",	_color_mouseover+_format_mouseover+"Pick up", fnc_ProcessCommands, COMMANDS.PICK_UP);
		fnc_Link(_x1+200, _y1+40,	(active_command == COMMANDS.LOOK_AT ? _color_selected : _color_normal)+_format_normal+"Look at",	_color_mouseover+_format_mouseover+"Look at", fnc_ProcessCommands, COMMANDS.LOOK_AT);
		fnc_Link(_x1+200, _y1+80,	(active_command == COMMANDS.GIVE ? _color_selected : _color_normal)+_format_normal+"Give",	_color_mouseover+_format_mouseover+"Give", fnc_ProcessCommands, COMMANDS.GIVE);
		
		
		
		
		// Draw inventory
		var _n = array_length(obj_Player.inventory);		
		var _initial_index = inventory_items_per_row * current_row;
		
		var _width = 48;
		var _height = 72;
		var _padding_inventory = 20;
		var _color = $777777;
		_x1 = _w-_width*(inventory_items_per_row)-_padding_inventory*(inventory_items_per_row)-3*_padding_inventory;
		_y1 = _h-140+_padding+30;
		
		var _mouseover_object = noone;
		
		for (var _i=_initial_index; _i<_initial_index + inventory_items_per_row; _i++) {
			var _x = _x1 + (_i-_initial_index)*_width + max(_i-_initial_index,0)*_padding_inventory;
			// Draw slot
			draw_roundrect_color(_x, _y1, _x+_width, _y1+_height, _color, _color, true);
			
			if (_i<_n) { // Draw and process actual item in the inventory				
				var _inv_item_id = obj_Player.inventory[_i];
				var _sprite_w = sprite_get_width(_inv_item_id.sprite_index);
				var _sprite_h = sprite_get_height(_inv_item_id.sprite_index);
				var _scale_w = _width/_sprite_w;
				var _scale_h = _height/_sprite_h;
				var _draw_scale = min(_scale_w, _scale_h);				
				draw_sprite_ext(_inv_item_id.sprite_index, _inv_item_id.image_index, _x+_width/2, _y1+_height, _draw_scale, _draw_scale, 0, c_white, 1);
				
				// Mouseover
				if (device_mouse_x_to_gui(0) >= _x && device_mouse_x_to_gui(0) <= _x+_width && device_mouse_y_to_gui(0) >= _y1 && device_mouse_y_to_gui(0) <= _y1+_height) {
					_mouseover_object = _inv_item_id;
					
					// Clicks - perform actions
					if (device_mouse_check_button_pressed(0, mb_left)) {
						
						with (_inv_item_id) {
							simulate_mb_left = true;
							event_perform(ev_step, 0);
						}
					}
				}
				
								
			}
		}
		
		
		
		// Handle scrollbars
		var _total_rows_needed = max(ceil(_n/inventory_items_per_row)-1, 0);
		
		var _alpha = current_row == 0 ? 0.3 : 1;
		draw_sprite_ext(spr_Arrows, 0, _x+_width+2*_padding_inventory, _y1+sprite_get_height(spr_Arrows)/2, 1, 1, 0, c_white, _alpha);
		if (device_mouse_check_button_pressed(0, mb_left) &&
			device_mouse_x_to_gui(0) >= _x+_width+2*_padding_inventory - sprite_get_width(spr_Arrows)/2 &&
			device_mouse_x_to_gui(0) <= _x+_width+2*_padding_inventory + sprite_get_width(spr_Arrows)/2 &&
			device_mouse_y_to_gui(0) >= _y1 &&
			device_mouse_y_to_gui(0) <= _y1+sprite_get_height(spr_Arrows)) {
			current_row = max(current_row-1, 0);
		}
		
		var _alpha = current_row == _total_rows_needed ? 0.3 : 1;
		draw_sprite_ext(spr_Arrows, 1, _x+_width+2*_padding_inventory, _y1+sprite_get_height(spr_Arrows), 1, 1, 0, c_white, _alpha);
		if (device_mouse_check_button_pressed(0, mb_left) &&
			device_mouse_x_to_gui(0) >= _x+_width+2*_padding_inventory - sprite_get_width(spr_Arrows)/2 &&
			device_mouse_x_to_gui(0) <= _x+_width+2*_padding_inventory + sprite_get_width(spr_Arrows)/2 &&
			device_mouse_y_to_gui(0) >= _y1+sprite_get_height(spr_Arrows) &&
			device_mouse_y_to_gui(0) <= _y1+2*sprite_get_height(spr_Arrows)) {
			current_row = min(current_row+1, _total_rows_needed);
		}
		
		// Draw active command text
		
		var _object_txt = "";
		var _sec_object_txt = "";
		
		if (active_command != noone) {
			if (Game.active_object != noone) {
				_object_txt = Game.active_object.logic.name;
				_sec_object_txt = _mouseover_object != noone ? _mouseover_object.logic.name : "";
			}
			else {
				_object_txt = _mouseover_object != noone ? _mouseover_object.logic.name : "";
				_sec_object_txt = "";
			}
			
			_scale = 0.3;
			var _format = "[fnt_Commands][fa_left][fa_bottom][scale,"+string(_scale)+"]";
			var _color = "[c_white]";
			var _color_shadow = "[c_black]";
			var _suffix = (active_command == COMMANDS.USE && active_object != noone && active_object.logic.use_combine ? command_suffix[COMMANDS.USE] : (Game.active_command == COMMANDS.GIVE && Game.active_object != noone ? command_suffix[COMMANDS.GIVE] : ""));
			type_formatted_shadow(_x1, _y1-10, _format+command_names[active_command]+" "+_object_txt+_suffix+_sec_object_txt, _color, _color_shadow);
		}
		else {
			if (Game.active_object != noone) {
				_object_txt = "";
			}
			else {
				_object_txt = _mouseover_object != noone ? _mouseover_object.logic.name : "";
			}
			_scale = 0.3;
			var _format = "[fnt_Commands][fa_left][fa_bottom][scale,"+string(_scale)+"]";
			var _colortxt = "[c_white]";
			var _color_shadow = "[c_black]";
			type_formatted_shadow(_x1, _y1-10, _format+_object_txt, _colortxt, _color_shadow);
		}
	}
	fnc_RestoreDrawParams();
}

function fnc_ProcessCommands(_command) {
	active_command = _command;
}


// GUI for TalkManager (when multiple options are presentI
function fnc_ProcessDialog(_index) {
	obj_TalkManager.reply_from_player = _index;		
}

function fnc_PushMessage(_index) {
	var _id = dialog[_index].id;
	_id.active_message = [];
	
	// Push message
	var _m = array_length(dialog[_index].dialog_text());
	for (var _i=0; _i<_m; _i++) {				
		array_push(_id.active_message, dialog[_index].dialog_text()[_i]);
	}

	_id.message_index = 0;
	_id.alarm[0] = -1;
		
	dialog_part_ended = false;	
}


#endregion

#region Debug

function fnc_DrawDebug() {
	if (Game.debug) {
		draw_set_color(c_black);
		draw_set_halign(fa_left);
		draw_set_valign(fa_top);	
		draw_set_font(fnt_Debug);
		draw_set_alpha(0.5);
		//draw_rectangle_color(0, 0, GUI_WIDTH, GUI_HEIGHT, c_black, c_black, c_black, c_black, false);
		draw_set_alpha(1);
		draw_set_color(c_white);
		draw_text(20, 20,	"DEBUG MODE");
		if (instance_exists(obj_Player)) {
			draw_text(20, 30, "Player: "+string(obj_Player.x)+","+string(obj_Player.y)+" / State="+obj_Player.state);
		}
		else {
			draw_text(20, 30, "Player not in room");	
		}
		var _spacing = 50;
		
		var _debug_data = [];
		
		array_push(_debug_data, "Room: "+string(room_width)+"x"+string(room_height)+" ("+string(round(room_width/room_height * 100)/100)+")");
		array_push(_debug_data, "Room speed: "+string(room_speed)+" Game speed: "+string(gamespeed_fps));
		array_push(_debug_data, "Requested scaling type: "+string(SELECTED_SCALING)+" ("+
						(SELECTED_SCALING==SCALING_TYPE.RESOLUTION_ADAPTED_TO_WINDOW ? "Normal mode (resolution scaled to window)" : 
						(SELECTED_SCALING==SCALING_TYPE.RESOLUTION_INDEPENDENT_OF_WINDOW ? "Resolution independent of window, unscaled" : "Resolution independent of of window, scaled")+")"));
		array_push(_debug_data, "Requested base resolution: "+string(BASE_RESOLUTION_W)+"x"+string(BASE_RESOLUTION_H));
		array_push(_debug_data, "Actual base resolution: "+string(adjusted_resolution_width)+"x"+string(adjusted_resolution_height));
		array_push(_debug_data, "Requested window size: "+string(BASE_WINDOW_SIZE_W)+"x"+string(BASE_WINDOW_SIZE_H));
		array_push(_debug_data, "Actual window size: "+string(adjusted_window_width)+"x"+string(adjusted_window_height));
		array_push(_debug_data, "App Surface: "+string(surface_get_width(application_surface))+"x"+string(surface_get_height(application_surface))+" ("+string(round(surface_get_width(application_surface)/surface_get_height(application_surface) * 100)/100)+")");
		array_push(_debug_data, "Window: "+string(window_get_width())+"x"+string(window_get_height())+" ("+string(round(window_get_width()/window_get_height() * 100)/100)+")");
		array_push(_debug_data, "Display: "+string(display_get_width())+"x"+string(display_get_height())+" ("+string(round(display_get_width()/display_get_height() * 100)/100)+")");
		array_push(_debug_data, "GUI Layer (with GM function): "+string(display_get_gui_width())+"x"+string(display_get_gui_height())+" ("+string(round(display_get_gui_width()/display_get_gui_height() * 100)/100)+")");
		array_push(_debug_data, "GUI Layer (with helper functions!): "+string(GUI_WIDTH)+"x"+string(GUI_HEIGHT)+" ("+string(round(GUI_WIDTH/GUI_HEIGHT * 100)/100)+")");
		array_push(_debug_data, "Mouse: "+string(mouse_x)+","+string(mouse_y));
		array_push(_debug_data, "Device mouse 0: "+string(device_mouse_x(0))+","+string(device_mouse_y(0)));
		array_push(_debug_data, "Device mouse 0 GUI: "+string(device_mouse_x_to_gui(0))+","+string(device_mouse_y_to_gui(0)));
		array_push(_debug_data, "Paused: "+string(paused));
		array_push(_debug_data, "FPS: "+string(fps_real) + "/" + string(fps));
		
		var _n = array_length(_debug_data);
		for (var _i=0; _i<_n; _i++) {
			draw_text(20, _spacing+_i*15, _debug_data[_i]);
			//show_debug_message(_debug_data[_i]);
		}
	}
}

#endregion

#region Utility/Other

function fnc_Link(_x, _y, _text, _text_mouseover, _callback, _param) {
	var _draw_data_normal = type_formatted(_x, _y, _text, false);
	//var _draw_data_mouseover = type_formatted(_x, _y, _text_mouseover, false);

	var _mousex = device_mouse_x_to_gui(0);
	var _mousey = device_mouse_y_to_gui(0);

	var _bbox_coords = _draw_data_normal.bbox(_x, _y);

	var _mouseover = _mousex >= _bbox_coords[0] && _mousey >= _bbox_coords[1] && _mousex <= _bbox_coords[2] && _mousey <= _bbox_coords[3];
	//var _mouseover = _mousex >= _draw_data_normal.bbox_x1 && _mousey >= _draw_data_normal.bbox_y1 && _mousex <= _draw_data_normal.bbox_x2 && _mousey <= _draw_data_normal.bbox_y2;
	var _click = device_mouse_check_button_pressed(0, mb_left);
	
	if (Game.debug) {
		draw_set_color(c_green);
		//draw_rectangle(_draw_data_normal.bbox_x1, _draw_data_normal.bbox_y1, _draw_data_normal.bbox_x2, _draw_data_normal.bbox_y2, false);
		draw_rectangle(_bbox_coords[0], _bbox_coords[1], _bbox_coords[2], _bbox_coords[3], false);
	}
	
	if (_click && _mouseover) {
		type_formatted(_x, _y, _text_mouseover);
		if (_callback != noone) {
			//script_execute(_callback, _param);
			_callback(_param);
		}
	}
	else if (_mouseover) {
		type_formatted(_x, _y, _text_mouseover);
	}
	else {
		type_formatted(_x, _y, _text);
	}

}

function fnc_ExecuteMenu(_param) {
	var _function = (asset_get_index("fnc_Menu_"+string(_param)));
	_function();
}

function fnc_ExecuteOptions(_param) {
	var _function = script_execute(asset_get_index("fnc_Options_"+string(_param)));
	_function();
}

function fnc_TryAgain() {
	paused = false;
	room_restart();	
}
	
#endregion