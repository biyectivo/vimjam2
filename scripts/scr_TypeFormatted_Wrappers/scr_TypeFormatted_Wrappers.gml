
//function fnc_draw_text_ext_transformed_color(_x,_y,_string,_sep,_w,_xscale,_yscale,_angle,_c1,_c2,_c3,_c4,_alpha) {
function fnc_draw_text_ext_transformed_color(_array) {
	var _x = _array[0];
	var _y = _array[1];
	var _string = _array[2];
	var _sep = _array[3];
	var _w = _array[4];
	var _xscale = _array[5];
	var _yscale = _array[6];
	var _angle = _array[7];
	var _c1 = _array[8];
	var _c2 = _array[9];
	var _c3 = _array[10];
	var _c4 = _array[11];
	var _alpha = _array[12];
	
	var _c = draw_get_color();
	var _a = draw_get_alpha();
	if (_sep == -1 && _w == -1 && _xscale == 1 && _yscale == 1 && _angle == 0 && _c1==_c && _c2==_c && _c3==_c && _c4==_c && abs(_alpha-_a)<=TYPE_FORMATTED_ALPHA_CHANGE_TOLERANCE) {
		draw_text(_x, _y, _string);
	}
	else if (_sep == -1 && _w == -1 && _xscale == 1 && _yscale == 1 && _angle == 0) {
		draw_text_color(_x, _y, _string, _c1, _c2, _c3, _c4, _alpha);
	}
	else if (_xscale == 1 && _yscale == 1 && _angle == 1 && _c1==_c && _c2==_c && _c3==_c && _c4==_c && abs(_alpha-_a)<=TYPE_FORMATTED_ALPHA_CHANGE_TOLERANCE) {
		draw_text_ext(_x,_y,_string,_sep,_w);
	}
	else if (_xscale == 1 && _yscale == 1 && _angle == 1) {
		draw_text_ext_color(_x,_y,_string,_sep,_w,_c1,_c2,_c3,_c4,_alpha);
	}
	else if (_sep == -1 && _w == -1) {
		draw_text_transformed(_x, _y, _string, _xscale, _yscale, _angle);
	}
	else {
		draw_text_ext_transformed(_x,_y,_string,_sep,_w,_xscale,_yscale,_angle);
	}
}


//function fnc_draw_text_ext_transformed(_x,_y,_string,_sep,_w,_xscale,_yscale,_angle) {
function fnc_draw_text_ext_transformed(_array) {
	var _x = _array[0];
	var _y = _array[1];
	var _string = _array[2];
	var _sep = _array[3];
	var _w = _array[4];
	var _xscale = _array[5]; 
	var _yscale = _array[6];
	var _angle = _array[7];
	
	if (_sep == -1 && _w == -1 && _xscale == 1 && _yscale == 1 && _angle == 0) {
		draw_text(_x, _y, _string);
	}
	else if (_xscale == 1 && _yscale ==1 && _angle == 1) {
		draw_text_ext(_x,_y,_string,_sep,_w);
	}
	else if (_sep == -1 && _w == -1) {
		draw_text_transformed(_x, _y, _string, _xscale, _yscale, _angle);
	}
	else {
		draw_text_ext_transformed(_x,_y,_string,_sep,_w,_xscale,_yscale,_angle);
	}
}

//function fnc_draw_set_alpha(_alpha) {
function fnc_draw_set_alpha(_arg) {
	if (is_array(_arg)) {
		var _alpha = _arg[0];
	}
	else {
		var _alpha = _arg;
	}
	if (abs(draw_get_alpha() - _alpha) > TYPE_FORMATTED_ALPHA_CHANGE_TOLERANCE) {
		draw_set_alpha(_alpha);	
	}
}

//function fnc_draw_set_color(_color_string) {	
function fnc_draw_set_color(_arg) {
	if (is_array(_arg)) {
		var _color_string = _arg[0];
	}
	else {
		var _color_string = _arg;
	}
	
	var _col = fnc_parse_color(_color_string);
	if (draw_get_color() != _col) {
		draw_set_color(_col);
	}
}

function fnc_parse_color(_color_string) {
	/*
	if (string_copy(_color_string, 1, 1) == "#") { // Change color, custom - RGB version
		var _col = make_color_rgb(	real(base_convert(string_copy(_color_string, 2, 2), 16, 10)), 
									real(base_convert(string_copy(_color_string, 4, 2), 16, 10)),
									real(base_convert(string_copy(_color_string, 6, 2), 16, 10)));
		
	}
	else if (string_copy(_color_string, 1, 1) == "$") { // Change color, custom
		var _col = make_color_rgb(	real(base_convert(string_copy(_color_string, 6, 2), 16, 10)), 
									real(base_convert(string_copy(_color_string, 4, 2), 16, 10)),
									real(base_convert(string_copy(_color_string, 2, 2), 16, 10)));		
	}
	else if (string_copy(_color_string, 1, 8) == "c_random") {		
		var _col = make_color_rgb(irandom_range(0,255), irandom_range(0,255), irandom_range(0,255));		
	}
	else if (string_copy(_color_string, 1, 2) == "c_") { // Change color, built-in constant
		if (!is_undefined(ds_map_find_value(global.tf_colors, _color_string))) {
			_col = global.tf_colors[? _color_string];			
		}
	}
	else {
		_col = -1;	
	}
	
	return _col;*/
	if (is_real(_color_string)) {
		return _color_string;
	}
	else if (_color_string == "c_random") {
		return make_color_rgb(irandom_range(0,255), irandom_range(0,255), irandom_range(0,255));
	}
	else if (!is_undefined(ds_map_find_value(global.tf_colors, _color_string))) {
		return global.tf_colors[? _color_string];
	}
	else {
		return -1;	
	}
}

//function fnc_draw_set_font(font) {	
function fnc_draw_set_font(_arg) {
	if (is_array(_arg)) {
		var _font = _arg[0];
	}
	else {
		var _font = _arg;
	}
	if (draw_get_font() != _font) {		
		draw_set_font(_font);
	}
}
