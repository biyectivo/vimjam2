{
  "bboxMode": 0,
  "collisionKind": 1,
  "type": 0,
  "origin": 7,
  "preMultiplyAlpha": false,
  "edgeFiltering": false,
  "collisionTolerance": 0,
  "swfPrecision": 2.525,
  "bbox_left": 3,
  "bbox_right": 44,
  "bbox_top": 63,
  "bbox_bottom": 95,
  "HTile": false,
  "VTile": false,
  "For3D": false,
  "width": 48,
  "height": 96,
  "textureGroupId": {
    "name": "Default",
    "path": "texturegroups/Default",
  },
  "swatchColours": null,
  "gridX": 0,
  "gridY": 0,
  "frames": [
    {"compositeImage":{"FrameId":{"name":"e08d2dbb-abb8-467f-a185-fb03881e21fa","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e08d2dbb-abb8-467f-a185-fb03881e21fa","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":{"name":"cc2878d8-84ef-4126-a2be-f3813bfb621e","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Serena_Outfit_Move","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"e08d2dbb-abb8-467f-a185-fb03881e21fa","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"69a8241c-2ef8-4094-aee2-97da0863e5e6","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"69a8241c-2ef8-4094-aee2-97da0863e5e6","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":{"name":"cc2878d8-84ef-4126-a2be-f3813bfb621e","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Serena_Outfit_Move","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"69a8241c-2ef8-4094-aee2-97da0863e5e6","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"a41b80da-b7ed-4f61-b609-425f83304b01","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"a41b80da-b7ed-4f61-b609-425f83304b01","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":{"name":"cc2878d8-84ef-4126-a2be-f3813bfb621e","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Serena_Outfit_Move","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"a41b80da-b7ed-4f61-b609-425f83304b01","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"cc77af6d-3d05-4ead-bd8e-efad655758ef","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"cc77af6d-3d05-4ead-bd8e-efad655758ef","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":{"name":"cc2878d8-84ef-4126-a2be-f3813bfb621e","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Serena_Outfit_Move","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"cc77af6d-3d05-4ead-bd8e-efad655758ef","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4563f516-80d9-4ea6-8270-0eeebbcbc4f3","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4563f516-80d9-4ea6-8270-0eeebbcbc4f3","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":{"name":"cc2878d8-84ef-4126-a2be-f3813bfb621e","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Serena_Outfit_Move","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"4563f516-80d9-4ea6-8270-0eeebbcbc4f3","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ab2e9545-598f-4767-b188-ae90b9f2d72a","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ab2e9545-598f-4767-b188-ae90b9f2d72a","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":{"name":"cc2878d8-84ef-4126-a2be-f3813bfb621e","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Serena_Outfit_Move","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"ab2e9545-598f-4767-b188-ae90b9f2d72a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0aa5674c-8421-4bed-928c-5a5e1fbb1461","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0aa5674c-8421-4bed-928c-5a5e1fbb1461","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":{"name":"cc2878d8-84ef-4126-a2be-f3813bfb621e","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Serena_Outfit_Move","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"0aa5674c-8421-4bed-928c-5a5e1fbb1461","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ee0dc6ce-ffa3-49b8-a97a-c2f38727827a","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ee0dc6ce-ffa3-49b8-a97a-c2f38727827a","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":{"name":"cc2878d8-84ef-4126-a2be-f3813bfb621e","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Serena_Outfit_Move","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"ee0dc6ce-ffa3-49b8-a97a-c2f38727827a","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"f746f77b-b94b-46c3-84bc-65ba5ff63fb1","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"f746f77b-b94b-46c3-84bc-65ba5ff63fb1","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":{"name":"cc2878d8-84ef-4126-a2be-f3813bfb621e","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Serena_Outfit_Move","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"f746f77b-b94b-46c3-84bc-65ba5ff63fb1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0eb089f8-d031-4a01-bb20-15844d8a187d","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0eb089f8-d031-4a01-bb20-15844d8a187d","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":{"name":"cc2878d8-84ef-4126-a2be-f3813bfb621e","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Serena_Outfit_Move","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"0eb089f8-d031-4a01-bb20-15844d8a187d","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"72197672-fd2c-48e8-ba16-89f1ab653267","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"72197672-fd2c-48e8-ba16-89f1ab653267","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":{"name":"cc2878d8-84ef-4126-a2be-f3813bfb621e","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Serena_Outfit_Move","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"72197672-fd2c-48e8-ba16-89f1ab653267","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"47cbcddc-969e-49e2-90ec-7a3f27dbce60","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"47cbcddc-969e-49e2-90ec-7a3f27dbce60","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":{"name":"cc2878d8-84ef-4126-a2be-f3813bfb621e","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Serena_Outfit_Move","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"47cbcddc-969e-49e2-90ec-7a3f27dbce60","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"9a077838-711d-433c-aa18-1e6d2cd0fa09","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"9a077838-711d-433c-aa18-1e6d2cd0fa09","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":{"name":"cc2878d8-84ef-4126-a2be-f3813bfb621e","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Serena_Outfit_Move","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"9a077838-711d-433c-aa18-1e6d2cd0fa09","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ff75f3fe-f25e-451c-9b4a-146f4292d480","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ff75f3fe-f25e-451c-9b4a-146f4292d480","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":{"name":"cc2878d8-84ef-4126-a2be-f3813bfb621e","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Serena_Outfit_Move","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"ff75f3fe-f25e-451c-9b4a-146f4292d480","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"4563073e-5af8-4ec7-a81f-f1070ca9babe","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"4563073e-5af8-4ec7-a81f-f1070ca9babe","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":{"name":"cc2878d8-84ef-4126-a2be-f3813bfb621e","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Serena_Outfit_Move","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"4563073e-5af8-4ec7-a81f-f1070ca9babe","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d7501086-aa9d-4044-be37-79ee2127b07e","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d7501086-aa9d-4044-be37-79ee2127b07e","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":{"name":"cc2878d8-84ef-4126-a2be-f3813bfb621e","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Serena_Outfit_Move","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"d7501086-aa9d-4044-be37-79ee2127b07e","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"65aa96c3-375d-4d42-bca7-d0bc04fe06ec","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"65aa96c3-375d-4d42-bca7-d0bc04fe06ec","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":{"name":"cc2878d8-84ef-4126-a2be-f3813bfb621e","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Serena_Outfit_Move","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"65aa96c3-375d-4d42-bca7-d0bc04fe06ec","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"d334ad32-2421-4853-be78-8042d508aba2","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"d334ad32-2421-4853-be78-8042d508aba2","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":{"name":"cc2878d8-84ef-4126-a2be-f3813bfb621e","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Serena_Outfit_Move","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"d334ad32-2421-4853-be78-8042d508aba2","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"96ff3514-ba0d-4ad9-9ee5-179b52ad4620","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"96ff3514-ba0d-4ad9-9ee5-179b52ad4620","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":{"name":"cc2878d8-84ef-4126-a2be-f3813bfb621e","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Serena_Outfit_Move","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"96ff3514-ba0d-4ad9-9ee5-179b52ad4620","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"188f99ac-f143-48b2-b812-c3f6179248bc","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"188f99ac-f143-48b2-b812-c3f6179248bc","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":{"name":"cc2878d8-84ef-4126-a2be-f3813bfb621e","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Serena_Outfit_Move","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"188f99ac-f143-48b2-b812-c3f6179248bc","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"e155ee07-d9ff-4efa-bdb9-873d4d1be6a8","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"e155ee07-d9ff-4efa-bdb9-873d4d1be6a8","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":{"name":"cc2878d8-84ef-4126-a2be-f3813bfb621e","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Serena_Outfit_Move","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"e155ee07-d9ff-4efa-bdb9-873d4d1be6a8","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"352758e3-b2db-461c-9f40-a5e05f0819f1","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"352758e3-b2db-461c-9f40-a5e05f0819f1","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":{"name":"cc2878d8-84ef-4126-a2be-f3813bfb621e","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Serena_Outfit_Move","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"352758e3-b2db-461c-9f40-a5e05f0819f1","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"0acc7b20-b90e-4608-aaba-88116daddfcd","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"0acc7b20-b90e-4608-aaba-88116daddfcd","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":{"name":"cc2878d8-84ef-4126-a2be-f3813bfb621e","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Serena_Outfit_Move","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"0acc7b20-b90e-4608-aaba-88116daddfcd","tags":[],"resourceType":"GMSpriteFrame",},
    {"compositeImage":{"FrameId":{"name":"ea892283-dfd9-49ba-b694-11bd171dac9c","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":null,"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},"images":[
        {"FrameId":{"name":"ea892283-dfd9-49ba-b694-11bd171dac9c","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"LayerId":{"name":"cc2878d8-84ef-4126-a2be-f3813bfb621e","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"","tags":[],"resourceType":"GMSpriteBitmap",},
      ],"parent":{"name":"spr_Serena_Outfit_Move","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","name":"ea892283-dfd9-49ba-b694-11bd171dac9c","tags":[],"resourceType":"GMSpriteFrame",},
  ],
  "sequence": {
    "spriteId": {"name":"spr_Serena_Outfit_Move","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},
    "timeUnits": 1,
    "playback": 1,
    "playbackSpeed": 10.0,
    "playbackSpeedType": 0,
    "autoRecord": true,
    "volume": 1.0,
    "length": 24.0,
    "events": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MessageEventKeyframe>",},
    "moments": {"Keyframes":[],"resourceVersion":"1.0","resourceType":"KeyframeStore<MomentsEventKeyframe>",},
    "tracks": [
      {"name":"frames","spriteId":null,"keyframes":{"Keyframes":[
            {"id":"5fff97b1-be83-46da-98eb-fb883d0a21d9","Key":0.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e08d2dbb-abb8-467f-a185-fb03881e21fa","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a244f4a5-2727-4d33-8d94-953bf0fd9e93","Key":1.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"69a8241c-2ef8-4094-aee2-97da0863e5e6","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"e478db59-69c0-4929-9771-43c51a52b579","Key":2.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"a41b80da-b7ed-4f61-b609-425f83304b01","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"a64719be-e0ce-478d-bf47-41cf2374a13b","Key":3.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"cc77af6d-3d05-4ead-bd8e-efad655758ef","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"d0daa3e7-dd01-4e10-9786-905207944461","Key":4.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4563f516-80d9-4ea6-8270-0eeebbcbc4f3","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"6f6426cc-55b6-43b3-973d-62a7889fcf18","Key":5.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ab2e9545-598f-4767-b188-ae90b9f2d72a","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"20a7c2ba-5525-464e-aa60-1987c8f8825d","Key":6.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0aa5674c-8421-4bed-928c-5a5e1fbb1461","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"7b966465-0ab3-45f6-8219-98d9f2927318","Key":7.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ee0dc6ce-ffa3-49b8-a97a-c2f38727827a","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"cf0c6b2e-4900-4615-8965-2bc0474bad33","Key":8.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"f746f77b-b94b-46c3-84bc-65ba5ff63fb1","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f1fe29da-839c-4457-baa9-5abcd7058274","Key":9.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0eb089f8-d031-4a01-bb20-15844d8a187d","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b1ecb59b-a6da-40c1-8d03-e9e9cc714174","Key":10.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"72197672-fd2c-48e8-ba16-89f1ab653267","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5ed14893-f9a8-4a1a-9713-eaea5bd59641","Key":11.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"47cbcddc-969e-49e2-90ec-7a3f27dbce60","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"f2765b7a-cf0a-4876-9faf-ce9ddc87b41a","Key":12.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"9a077838-711d-433c-aa18-1e6d2cd0fa09","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"14cbd294-cbef-434b-a7a2-fac699ebe496","Key":13.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ff75f3fe-f25e-451c-9b4a-146f4292d480","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b067a6a8-9e8d-49ac-a0f3-b7c24239f22b","Key":14.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"4563073e-5af8-4ec7-a81f-f1070ca9babe","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"4ede5585-491c-4d02-9ff9-25c9df644e4b","Key":15.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d7501086-aa9d-4044-be37-79ee2127b07e","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"c584b921-3b93-49d1-96ed-95e5aa6292d5","Key":16.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"65aa96c3-375d-4d42-bca7-d0bc04fe06ec","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"499e5774-30fb-4a52-88d7-14b0e2ebfd65","Key":17.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"d334ad32-2421-4853-be78-8042d508aba2","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"b4350beb-dae6-4a02-aaec-50f5596abc53","Key":18.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"96ff3514-ba0d-4ad9-9ee5-179b52ad4620","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"5f6bd973-2c34-4d71-bcce-132c18758a68","Key":19.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"188f99ac-f143-48b2-b812-c3f6179248bc","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"719b0847-6463-4c9f-a590-313858f4c663","Key":20.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"e155ee07-d9ff-4efa-bdb9-873d4d1be6a8","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"8a22b383-bbcc-4d85-9135-38cb22709097","Key":21.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"352758e3-b2db-461c-9f40-a5e05f0819f1","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"bdb7cf43-f864-4823-90ba-a1f606cff90c","Key":22.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"0acc7b20-b90e-4608-aaba-88116daddfcd","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
            {"id":"11bd5de5-30e5-4669-98dd-193530ab1f30","Key":23.0,"Length":1.0,"Stretch":false,"Disabled":false,"IsCreationKey":false,"Channels":{"0":{"Id":{"name":"ea892283-dfd9-49ba-b694-11bd171dac9c","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},"resourceVersion":"1.0","resourceType":"SpriteFrameKeyframe",},},"resourceVersion":"1.0","resourceType":"Keyframe<SpriteFrameKeyframe>",},
          ],"resourceVersion":"1.0","resourceType":"KeyframeStore<SpriteFrameKeyframe>",},"trackColour":0,"inheritsTrackColour":true,"builtinName":0,"traits":0,"interpolation":1,"tracks":[],"events":[],"modifiers":[],"isCreationTrack":false,"resourceVersion":"1.0","tags":[],"resourceType":"GMSpriteFramesTrack",},
    ],
    "visibleRange": null,
    "lockOrigin": false,
    "showBackdrop": true,
    "showBackdropImage": false,
    "backdropImagePath": "",
    "backdropImageOpacity": 0.5,
    "backdropWidth": 1366,
    "backdropHeight": 768,
    "backdropXOffset": 0.0,
    "backdropYOffset": 0.0,
    "xorigin": 24,
    "yorigin": 96,
    "eventToFunction": {},
    "eventStubScript": null,
    "parent": {"name":"spr_Serena_Outfit_Move","path":"sprites/spr_Serena_Outfit_Move/spr_Serena_Outfit_Move.yy",},
    "resourceVersion": "1.3",
    "name": "spr_Serena_Outfit_Move",
    "tags": [],
    "resourceType": "GMSequence",
  },
  "layers": [
    {"visible":true,"isLocked":false,"blendMode":0,"opacity":100.0,"displayName":"default","resourceVersion":"1.0","name":"cc2878d8-84ef-4126-a2be-f3813bfb621e","tags":[],"resourceType":"GMImageLayer",},
  ],
  "nineSlice": null,
  "parent": {
    "name": "NPC",
    "path": "folders/Sprites/NPC.yy",
  },
  "resourceVersion": "1.0",
  "name": "spr_Serena_Outfit_Move",
  "tags": [],
  "resourceType": "GMSprite",
}