//
// SinCity - like shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

void main() {
	vec4 base_color = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	
    base_color.rgb = pow(base_color.rgb, vec3(0.45)); // Gamma correction
	
	// NTSC
	const vec3 luminance_coefficients = vec3(0.299, 0.587, 0.114);
	float average_float = dot(base_color.rgb, luminance_coefficients);
	
	
	float weight_red = smoothstep(0.1, 0.25, base_color.r - average_float);
		
	average_float = pow(average_float * 1.1, 2.0);
	
	vec3 out_color = mix(vec3(average_float), base_color.rgb * vec3(1.1, 0.5, 0.5), weight_red);
		
	gl_FragColor = vec4(pow(out_color, vec3(2.2)), base_color.a);
}
