//
// DuoTone shader - sepia, etc.
// v_vColour is at the end, not at the beginning (base_color) in order to allow the blend color to apply over the effect (and not under)
// if v_vColour is left at the base_color definition, it will not matter
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform float x;

void main() {
    vec4 base_color = texture2D( gm_BaseTexture, v_vTexcoord );
	// NTSC:
	const vec3 luminance_coefficients = vec3(0.299, 0.587, 0.114);
	vec3 average = vec3(dot(base_color.rgb, luminance_coefficients));
	vec4 out_color = vec4(average, base_color.a);
	gl_FragColor = v_vColour * out_color;
}
