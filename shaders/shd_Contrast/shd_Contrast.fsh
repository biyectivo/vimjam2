//
// Contrast
// increasing contrast means making darks darker and lights lighter, BUT...
// decreasing contrast means making darks lighter and lights darker.
// 0 contrast is a line from (0, 0.5) to (1, 0.5) - all the same gray
// 1 contrast is y=0 from 0 to 0.5 and y=1 from 0.5 to 1
// alpha contrast (in (0,1)) is a stepwise function:
// y=0 from x=0 to alpha
// y = mx+b from alpha to 1-alpha
// y=1 from 1-alpha to 1
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform float contrast;

void main()
{
    vec4 base_col = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	base_col = vec4( (base_col.rgb - 0.5) * contrast + 0.5 , base_col.a);
	gl_FragColor = base_col;
}
