//
// Desaturation shader: strength = 0.0 means original, strength = 1.0 means black/white
// if v_vColour is at the top, first it receives the vertex colors (single or interpolated with draw_sprite_general) and then desaturates.
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform float strength;

void main()
{
    vec4 base_col = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	
	// luminance: easiest is average, some say use the NTSC conversion:
	float average = 0.299 * base_col.r + 0.587 * base_col.g + 0.114 * base_col.b;
	//float average = (base_col.r + base_col.g + base_col.b)/3.0;
	
	// without mix:
	//vec3 colors = (1.0-strength) * base_col.rgb + strength * average;
	// with mix: mix(a,b,k) = (1-k) * a + (k) * b => k=0 means a, k=1 means b
	vec3 colors = mix(base_col.rgb, vec3(average), strength);
	gl_FragColor = vec4(colors, base_col.a);
}
