//
// Red & B&W shader
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

void main() {
	vec4 base_color = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	
    //base_color.rgb = pow(base_color.rgb, vec3(0.45)); // Gamma correction
	
	// NTSC
	const vec3 luminance_coefficients = vec3(0.299, 0.587, 0.114);
	vec3 average = vec3(dot(base_color.rgb, luminance_coefficients));
	
	if (base_color.r > 0.5 && base_color.r > base_color.g + base_color.b) {
		base_color.rgb = base_color.rgb * vec3(1.5, 0.2, 0.2);
	}
	else {
		base_color.rgb = average;	
	}
		
	gl_FragColor = base_color;
}
