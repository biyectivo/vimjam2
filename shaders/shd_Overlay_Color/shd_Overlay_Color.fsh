//
// Simple overlay shader with color
//
varying vec2 v_vTexcoord;
// we don't need the vertex color, we will use the color
//varying vec4 v_vColour;

uniform vec3 overlay_color;

void main()
{
    vec4 base_color = texture2D( gm_BaseTexture, v_vTexcoord );
	
	// NTSC
	const vec3 luminance_coefficients = vec3(0.299, 0.587, 0.114);
	float luminance = dot(base_color.rgb, luminance_coefficients);
	
	if (luminance > 0.5) {
		base_color.rgb = 1.0 - (1.0 - 2.0*(base_color.rgb-0.5)) * (1.0-overlay_color);
	}
	else {
		base_color.rgb = (2.0 * base_color.rgb) * overlay_color;
	}
	
	gl_FragColor = base_color;
}

//(Target > 0.5) * (1 - (1-2*(Target-0.5)) * (1-Blend)) + (Target <= 0.5) * ((2*Target) * Blend)