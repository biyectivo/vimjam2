//
// Gamma shader - increase luminance but on an exponential curve, so very dark and very light stay the same
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform float gamma;

void main() {
    vec4 base_col = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	base_col = vec4(pow(base_col.rgb, vec3(1.0/gamma)), base_col.a);
    gl_FragColor = base_col;
}
