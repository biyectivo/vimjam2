//
// Color inversion - each color is set to 1-its value
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

void main()
{
    vec4 base_col = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	gl_FragColor = vec4(1.0-base_col.r, 1.0-base_col.g, 1.0-base_col.b, base_col.a);
}
