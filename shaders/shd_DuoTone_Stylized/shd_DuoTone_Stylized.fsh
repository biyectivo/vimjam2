//
// Duotone with two uniforms (light and dark)
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform vec3 duotone_light;
uniform vec3 duotone_dark;

void main() {
    vec4 base_color = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	
	// NTSC
	const vec3 luminance_coefficients = vec3(0.299, 0.587, 0.114);
	vec3 average = vec3(dot(base_color.rgb, luminance_coefficients));
	
	vec3 out_color = mix(duotone_light, duotone_dark, average);
	
    gl_FragColor = vec4(out_color, base_color.a);
}
