//
// Saturation shader - the inverse of desaturation
// The strength will need to be greater than 1 in order to saturate
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform float strength;
void main() {
    vec4 base_col = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	// luminance: easiest is average, some say use the NTSC conversion:
	float average = 0.299 * base_col.r + 0.587 * base_col.g + 0.114 * base_col.b;
	//float average = (base_col.r + base_col.g + base_col.b)/3.0;
	
	base_col = vec4( mix(vec3(average), base_col.rgb, strength), base_col.a);
	gl_FragColor = base_col;
}
