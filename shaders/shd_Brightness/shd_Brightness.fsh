//
// Brightness shader
// The luminance is increase by shifting the line upwards, this affects all colors
// Different from gamma, gamma will keep 0,0 and 1,1 in the same spot and handle exponential, affecting less the darker and lighter colors
//
varying vec2 v_vTexcoord;
varying vec4 v_vColour;

uniform float brightness;

void main() {
    vec4 base_col = v_vColour * texture2D( gm_BaseTexture, v_vTexcoord );
	base_col = vec4(base_col.rgb + brightness, base_col.a);
	gl_FragColor = base_col;
}
